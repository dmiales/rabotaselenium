package pages.employer.login;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class SignInHr {

    pages.employer.HrPage hrPage;

    //кнопка войти на главной hr нового лк
    private SelenideElement buttonSignIn             = $x("//div[@class='top-menu__profile']//div[2]");

    //поля и кнопки на странице авторизации
    public SelenideElement loginHRField              = $x("//input[@name='email']");
    public SelenideElement passwordHRField           = $x("//input[@name='password']");
    public SelenideElement noAccount                 = $x("//a[@class='r-btn r-btn_flat r-btn_link r-btn_outline r-btn_router r-btn_medium']"); //у меня нет аккаунта
    public SelenideElement logInButton               = $x("//button[@aria-label='Войти']");

    //
    public SelenideElement loginOldEmail             = $x("//input[@id='mail']");
    public SelenideElement passwordOld               = $x("//input[@id='password']");
    public SelenideElement buttondOld                = $x("//input[@class='login_btn']");
    public SelenideElement registrationButtonOld     =$x("//a[contains(text(),'Зарегистрироваться')]");
    //  Текст Ошибок
    public SelenideElement incorrectCredentials = $x("//div[@class='messages-pool__transition-group']");

    public SignInHr clickOnbuttonSignIn() {
        $(buttonSignIn).click();
        return page(SignInHr.class);
    }

    //  метод авторизации по логину и паролю
    public SignInHr logIn(String userLogin, String userPassword) {
        $(loginHRField).sendKeys(userLogin);
        $(passwordHRField).sendKeys(userPassword);
        $(logInButton).click();
        return page(SignInHr.class);
    }

    //у меня нет аккаунта
    public SignInHr clickOnNoAccount(){
        $(noAccount).click();
        return page(SignInHr.class);
    }

    public String getErrorIncorrectCredentials() {
        return $(incorrectCredentials).text();
    }

    public void succesSign (String login,
                            String password){
        Selenide.open("https://hr.dione.rabota.space");

        clickOnbuttonSignIn();
        logIn(login, password);
        sleep(5000);
    }

    public void oldSign (String email,
                         String password){
        hrPage = new pages.employer.HrPage();
        Selenide.open("https://hr.dione.rabota.space");
        hrPage.clickOnButtonCooke();
        By login = By.xpath("(//form/div[contains(@class, 'logpass_box')]/input)[1]");
        By pass = By.xpath("(//form/div[contains(@class, 'logpass_box')]/input)[2]");
        By btn = By.xpath("//input[contains(@class, 'login_btn')]");
        $(login).sendKeys(email);
        $(pass).sendKeys(password);
        $(btn).click();
    }

    public void oldEmail (String email){
        $(loginOldEmail).sendKeys(email);
    }

    public void oldPassword (String password){
        $(passwordOld).sendKeys(password);
    }
     public void oldButton (){
        $(buttondOld).click();
    }

    public void oldRegistration(){
        registrationButtonOld.click();
    }



}

