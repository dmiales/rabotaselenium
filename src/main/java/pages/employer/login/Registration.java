package pages.employer.login;

import com.codeborne.selenide.*;
import static com.codeborne.selenide.Selenide.*;

public class Registration {

    //поля и кнопки на странице регистрации
    private SelenideElement innCompany                  = $x("//input[@placeholder='Введите ИНН']");
    private SelenideElement nameHR                      = $x("//input[@placeholder='Введите имя']");
    private SelenideElement familyHR                    = $x("//input[@placeholder='Введите фамилию']");
    private SelenideElement emailHR                     = $x("//input[@placeholder='Введите ваш email']");
    private SelenideElement passwordHR                  = $x("//input[@placeholder='Введите пароль']");
    private SelenideElement telephoneHR                 = $x("//input[@placeholder='Введите телефон']");
    private SelenideElement zao                         = $x("//div[@name='company_business_entity_type_id']//div[@class='input-group__input']");
    private SelenideElement ooo                         = $x("//li[1]//a[1]//div[1]//div[1]");
    private SelenideElement nameCompanyHR               = $x("//input[@placeholder='Название']");
    private SelenideElement agencyType                  = $x("//button[@aria-label='Кадровое агентство']");
    private SelenideElement next                         = $x("//button[@class='registration-form__submit-button r-btn r-btn_full-width r-btn_large primary']");
    private SelenideElement sign                         = $x("//div[@class='registration-form__login-text']//a");
    private SelenideElement signSber                     = $x("//button[@aria-label='Войти по СберБизнес ID']");

    //ошибки
    private SelenideElement minInn                       = $x("//div[contains(text(),'ИНН может состоять только из 10 или 12 цифр')]");
    private SelenideElement incorrectInn                 = $x("//div[contains(text(),'Неправильный ИНН')]");
    private SelenideElement incorrectInnSym              = $x("//div[contains(text(),'ИНН может состоять только из цифр')]");
    private SelenideElement incorrectEmail              = $x("//div[contains(text(),'Введен некорректный почтовый адрес')]");
    private SelenideElement incorrectPasswordMin        = $x("//div[contains(text(),'Пароль должен содержать 8 знаков и более')]");
    private SelenideElement incorrectPasswordNum        = $x("//div[contains(text(),'Пароль должен содержать хотя бы одну строчную букву (a-z/а-я)')]");
    private SelenideElement incorrectPasswordNum2       = $x("//div[contains(text(),'Пароль должен содержать хотя бы одну прописную букву (A-Z/А-Я)')]");

    public Registration innCompanyHR (String inn){
        $(innCompany).sendKeys(inn);
        return page(Registration.class);
    }

    public Registration validRegistration (String name,
                                           String family,
                                           String email,
                                           String password,
                                           String telephone
    ) {
        $(nameHR).sendKeys(name);
        $(familyHR).sendKeys(family);
        $(emailHR).sendKeys(email);
        $(passwordHR).sendKeys(password);
        $(telephoneHR).sendKeys(telephone);
        return page(Registration.class);}

    public Registration agencyOnCLick(){
        agencyType.shouldHave(Condition.exist).click();
        return page(Registration.class);
    }
    public Registration nextOnClick(){
        next.click();
        return page(Registration.class);
    }
    public Registration signOnClick(){
        sign.click();
        return page(Registration.class);
    }
    public void sberButtonClick(){
        signSber.click();
    }
    //ошибки
    public Registration errors (String incorrectInn,
                                String incorrectEmail,
                                String incorrectPassword,
                                String incorrectPhone) {
        $(innCompany).sendKeys(incorrectInn);
        $(emailHR).sendKeys(incorrectEmail);
        $(passwordHR).sendKeys(incorrectPassword);
        $(telephoneHR).sendKeys(incorrectPhone);
        return page(Registration.class);
    }
    public String getErrorINN() {
        return $(minInn).getText();
    }
    public String getIncorrectInn(){
        return $(incorrectInn).getText();
    }
    public String getIncorrectInnSym(){
        return $(incorrectInnSym).getText();
    }
    public String getEmail(){
        return $(incorrectEmail).getText();
    }
    public String getPasswordMin() {
        return $(incorrectPasswordMin).getText();
    }
    public String getPasswordNum(){
        return $(incorrectPasswordNum).getText();
    }
    public String getPasswordNum2(){
        return $(incorrectPasswordNum2).getText();
    }

}
