package pages.employer.tariff;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class TariffHrOrder {

    //меню "Заказы и счета"
    private SelenideElement checkout                                 = $x("//a[@class='new_response t_blue']");

    //меню "Прайс-лист"
    private SelenideElement vacancyTab                               = $x("//div[@id='section_content']//a[1]");
    private SelenideElement resumeTab                                = $x("//div[@class='shop-top-lvl-tabs-wrapper']//a[2]");
    private SelenideElement driveTab                                 = $x("//div[@id='layout']//a[3]");
    private SelenideElement standartVacancyIncrement                 = $x("//div[@class='shop-group-cells-wrapper']//li[1]//a[@class='discount-counter discount-counter_plus']");
    private SelenideElement optimalVacancyIncrement                  = $x("//div[@class='shop-group-cells-wrapper']//li[3]//a[@class='discount-counter discount-counter_plus']");
    private SelenideElement premiumVacancyIncrement                  = $x("//div[@class='shop-group-cells-wrapper']//li[5]//a[@class='discount-counter discount-counter_plus']");
    private SelenideElement standartVacancyDecrement                 = $x("//div[@class='shop-group-cells-wrapper']//li[1]//a[@class='discount-counter discount-counter_minus']");
    private SelenideElement optimalVacancyDecrement                  = $x("//div[@class='shop-group-cells-wrapper']//li[3]//a[@class='discount-counter discount-counter_minus']");
    private SelenideElement premiumVacancyDecrement                  = $x("//div[@class='shop-group-cells-wrapper']//li[5]//a[@class='discount-counter discount-counter_minus']");
    private SelenideElement standartVacancyCount                     = $x("//div[@class='shop-group-cells-wrapper']//li[1]//input[@class='shop-group-choice-input']");
    private SelenideElement optimalVacancyCount                      = $x("//div[@class='shop-group-cells-wrapper']//li[3]//input[@class='shop-group-choice-input']");
    private SelenideElement premiumVacancyCount                      = $x("//div[@class='shop-group-cells-wrapper']//li[5]//input[@class='shop-group-choice-input']");
    private SelenideElement checkboxVacancyOnTheMain                 = $x("//div[@class='shop-group-double-columns-wrapper']//label[contains(text(), 'Вакансия на главной')]");
    private SelenideElement checkboxExpressVacancy                   = $x("//div[@class='shop-group-double-columns-wrapper']//label[contains(text(), 'Express.вакансия')]");
    private SelenideElement checkboxAnonymVacancyPremium             = $x("//div[@class='shop-group-double-columns-wrapper']//label[contains(text(), 'Анонимная вакансия-премиум')]");

    private SelenideElement cartNameTariff                           = $x("//div[contains(@class,'tariff-name')]");
    private SelenideElement buyButton                                = $x("//a[contains(@class,'order-btn')]");

    public void clickOnStandartVacancyIncrement(){
        standartVacancyIncrement.click();
    }
//можно ли будет вытащить отсюда переменную
    public void nameTariffCart(){
        String nameTariff;
        nameTariff = cartNameTariff.getValue();
    }

    public void buttonBuy(){
        buyButton.click();
    }

}
