package pages.employer.tariff;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;
import pages.employer.billingAndOrder.BillingAndOrder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.codeborne.selenide.Selenide.$x;

public class CrmPayment {

   // String number;

    private SelenideElement historyOrders                       = $x("//a[contains(text(), 'История заказов')]");
    private SelenideElement lastOrder                           = $x("//a[contains(text(), '0399336')]"); // сделать номер заказа как перемеенную (со страниы счета и заказы)
    private SelenideElement orderStatus                         = $x("//table[@class='table table-bordered']/tbody//td/select[@name='order_status']");
    private SelenideElement blockOrder                          = $x("//div[@id='search-company-card']//div[contains(@class,'panel-body')]//li[1]");
    private SelenideElement forScroll                           = $x("//button[contains(text(), 'Эффективность')]");

    private SelenideElement readyRun                            = $x("//select[@id='order_status']/option[contains(text(), 'готов к запуску')]");
    private SelenideElement datePayment                         = $x("//input[@id='receive_pay_date']");
    private SelenideElement saveButton                          = $x("//button[@id='saveStatus']");

    //ЛК страница заказы и счета
    private SelenideElement numberOrderField                    =$x("(//div[@class='lblock_info_block_title fleft']/a[@class='title_link large'])[1]");


    public void clickOnHistoryOrders(){
        historyOrders.waitUntil(Condition.visible, 10000).click();
    }

   public void clickOnLastOrder(){
      BillingAndOrder billingAndOrder = new BillingAndOrder();
      billingAndOrder.getNumberOrder();
       String number = billingAndOrder.number;
       System.out.println(number);
       $x("//a[contains(text(), ' " + number + "' )]").scrollTo().click();
   }

   public void scrollBlockOrder(){
        blockOrder.waitUntil(Condition.exist, 2000);
   }
    public void scroll(){
        forScroll.sendKeys(Keys.PAGE_DOWN);
    }

    public void clickOrderStatus(){
       // orderStatus.shouldBe(Condition.exist).scrollTo().click();
        orderStatus.selectOption("готов к запуску");
    }

    public void clickReadyRun(){
        readyRun.click();
    }
    public void clickOnDatePayment(){
        LocalDate localDate = LocalDate.now();
        String date = DateTimeFormatter.ofPattern("yyyy.MM.dd").format(localDate);
        datePayment.clear();
        datePayment.sendKeys(date);
        datePayment.click();
    }
    public void clickSaveButton(){
        saveButton.click();
    }



}
