package pages.employer.tariff;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class BillingTariff {
    private SelenideElement sendingDocuments                    = $x("//select[@id='accounting_documents_needed']");
    private SelenideElement paymentMethodYandex                 = $x("//input[@id='paymentTypeYandex_PC']");
    private SelenideElement papeworkFor                         = $x("//select[@id='payment_type']");
    private SelenideElement papeworkForJur                      = $x("//table[@class='privat_form vt']//option[2]");
    private SelenideElement confirmTheOrderButton               = $x("//input[@alt='Подтвердить заказ']");

    public void documentsJurist(){
        papeworkFor.click();
        papeworkForJur.click();
    }

    public void clickOnButtonConfirm(){
        confirmTheOrderButton.click();
    }

    public void paymentMethodCard(){
        paymentMethodYandex.click();
    }
}
