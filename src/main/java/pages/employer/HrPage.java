package pages.employer;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class HrPage {

    //главная
    private SelenideElement buttonCooke        = $x("//button[@class='hr-btn r-btn r-btn_medium primary hr-btn_colored']");

    //боковое меню работодателя
    private SelenideElement menu                     = $x("//div[@class='hr-app__side-menu']");
    private SelenideElement myVacancies              = $x("//a[@aria-label='Мои вакансии']");
    private SelenideElement myResponses              = $x("//a[@href='/responses']");
    private SelenideElement findResume               = $x("//a[@aria-label='Поиск резюме']");
    private SelenideElement baseResume               = $x("//a[@aria-label='Моя база резюме']");
    private SelenideElement shop                     = $x("//a[@aria-label='Прайс-лист']");
    private SelenideElement myCompany                = $x("//a[@href='/company']");
    private SelenideElement orderBill                = $x("//a[@aria-label='Заказы и счета']");

    //верхнее меню работодателя
    private SelenideElement createVacancyLk          = $x("//a[@aria-label='Добавить вакансию']"); //SPA
    private SelenideElement getCreateVacancyLkRetro  = $x("//a[@class='top-control-button top-control-button_blue add-vacancy']");
    private SelenideElement shopCart                 = $x("//a[@class='top-menu__shop-btn r-btn r-btn_icon r-btn_outline r-btn_large']");
    private SelenideElement userHRName               = $x("//div[@class='user-profile-menu__title']");

    //    метод получения имени авторизованного пользователя на главной странице
    public String getHRName() {
        return $(userHRName).text();
    }


    public HrPage clickOnButtonCooke() {
        buttonCooke.click();
        return page(HrPage.class);
    }

    public HrPage clickOnMyVacancies() {
        myVacancies.shouldHave(Condition.exist).click();
        return page(HrPage.class);
    }

    public HrPage clickOnMyResponses() {
        myResponses.click();
        return page(HrPage.class);
    }

    public HrPage clickOnFindResume() {
        findResume.click();
        return page(HrPage.class);
    }

    public HrPage clickOnBaseResume() {
        baseResume.click();
        return page(HrPage.class);
    }

    public HrPage clickOnShop() {
        shop.click();
        return page(HrPage.class);
    }

    public HrPage clickOnMyCompany() {
       myCompany.click();
        return page(HrPage.class);
    }

    public HrPage clickOnOrderBill() {
        orderBill.click();
        return page(HrPage.class);
    }

    public HrPage clickOnCreateVacancy() {
        createVacancyLk.shouldBe(Condition.exist).click();
        return page(HrPage.class);
    }

    public HrPage clickOnCreateVacancyRetro() {
        getCreateVacancyLkRetro.shouldBe(Condition.exist).click();
        return page(HrPage.class);
    }


    public HrPage clickoOnShopCart() {
        shopCart.click();
        return page(HrPage.class);
    }

    public HrPage clickOnUserName() {
        userHRName.click();
        return page(HrPage.class);
    }
}

