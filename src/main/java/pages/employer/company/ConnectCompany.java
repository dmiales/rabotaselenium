package pages.employer.company;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class ConnectCompany {

    //вкладка "присоединиться к компании"
    private SelenideElement buttonConnectCompany                    = $x("//a[@class='red_btn htrfix js-send-event-join']");
    private SelenideElement windowConnectCompany                    = $x ("//a[@class='red_btn js-send-event-join-popup']");
    private SelenideElement searchCity                              = $x("//input[@id='regionText']");
    private SelenideElement searchCompany                           = $x("//input[@id='employerName']");
    private SelenideElement buttonFound                             = $x("//input[@class='btn lgray']");


    public void connectCompany(){
        buttonConnectCompany.should(Condition.exist).click();
        windowConnectCompany.shouldBe(Condition.exist).click();

    }
}
