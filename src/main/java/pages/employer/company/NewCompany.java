package pages.employer.company;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class NewCompany {
    private SelenideElement newCompanyTab               = $x("//a[@class='red_tab inactive']");
    private SelenideElement nameCompany                 = $x("//input[@id='employerName']");
    private SelenideElement industryBusiness            = $x("//a[@class='text_12 on-form-cats-empty']");
    private SelenideElement industryIt                  = $x("//a[contains(text(),'IT')]");
    private SelenideElement industryItCall              = $x("//li[1]//ul[2]//li[1]//label[1]");
    private SelenideElement industryButtonDone          = $x("//a[@class='forest-cat-selected-button-ok']");
    private SelenideElement sendDocument                = $x("//select[@id='accounting_documents_needed']");
    private SelenideElement sendDocumentNoSent          = $x("//select[@id='accounting_documents_needed']/option[@value='no']");
    private SelenideElement nextButton                  = $x("//a[@id='company_form_submit']");

    public void clickOnTabNewCompany(){
        newCompanyTab.click();
    }
    public void companyName(String name){
        nameCompany.sendKeys(name);
    }
    public void industry(){
        industryBusiness.click();
        industryIt.shouldBe(Condition.exist).click();
        industryItCall.click();
        industryButtonDone.click();
    }

    public void sendDocuments(){
        sendDocument.click();
        sendDocumentNoSent.click();
    }

    public void clickOnNextButton(){
        nextButton.click();
    }
}
