package pages.employer.vacancy;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class CreateVacancy {

    pages.employer.HrPage hrPage;

    //  поля и кнопки на странице создания вакансии
    private SelenideElement titleVacancyEl      = $x("//input[@name='custom_position']");
    private SelenideElement titleList           = $x("//div[@id='Autocomplete_b9771']");
    private SelenideElement titleListFirst      = $x("//div[@class='autocomplete']/div[1]");
    private SelenideElement titleCheckBox       = $x("//li[3]//label[1]");
    private SelenideElement salaryFrom          = $x("//input[@name='salary_from']");
    private SelenideElement salaryTo            = $x("//input[@name='salary_to']");
    private SelenideElement experienceEl        = $x("//select[@id='offer_experience_year_count']");
    private SelenideElement getExperienceOneYear= $x("//*[@id='offer_experience_year_count']/option[5]");
    private SelenideElement specializashionEl   = $x("//span[@class='specialization-placeholder']");
    private SelenideElement specializCheck      =$x("//div[@class='specialization-list checkbox-style']");
    private SelenideElement specialCheckBox     = $x("//div[@id='dialog']//label[1]");
    private SelenideElement specialSave         = $x("//button[@class='btn-red mr_30']");
    private SelenideElement contactName         = $x("//input[@name='contact_fio']");
    private SelenideElement shedulesPolniyDen   = $x("//table[1]//tbody[1]//tr[2]//td[2]//div[1]//div[1]//label[1]//input[1]"); //поменять
    private SelenideElement descriptionEl       = $x("//tr/td[../td/text()='Описание вакансии']//iframe");
    private SelenideElement rubricEl            =$x("//span[@class='on-form-cats-empty-placeholder']");
    private SelenideElement save                = $x(" //a[@id='save_vacancy_button']");
    private SelenideElement addPlaceWork        =$x("//a[@id='vacancyAddressPopupLink']");
    private SelenideElement addPlaceWorkAddress = $x("//input[@class='w100 address address_suggester suggestions-input']");
    private SelenideElement addPlaceCreateButton= $x("//input[@class='btn-red create_address']");
    private SelenideElement addPlaceWorkRegion  = $x("//a[@id='addressRegionPopupLink']");
    private SelenideElement workRegionMSK       =$x(" //div[@class='clearfix rel mb_10']//div[1]//div[1]//label[1]//div[1]//input[1]");
    private SelenideElement workPlaceDoneButton = $x(" //input[@class='js-close b-red-button']");

    //ошибки
    private SelenideElement titleError          =$x("//span[@id='custom_position_err']");
    private SelenideElement experienceError     =$x("//label[@id='offer_experience_year_count_err']");
    private SelenideElement descriptionError    =$x("//label[@id='description_err']");
    private SelenideElement sheduleError        =$x("//label[@id='operating_schedule_err']");
    private SelenideElement rubrikError         =$x("//div[@id='errTrade']");
    private SelenideElement salaryFromError     =$x("Нижний уровень зарплаты не может быть больше верхнего");
    private SelenideElement SalaryFromError2    =$x("Попробуйте увеличить сумму");

    public CreateVacancy titleVacancy(String title){
        $(titleVacancyEl).setValue(title);
        $(titleListFirst).waitUntil(Condition.visible, 1000).click();
        return page(CreateVacancy.class);
    }

    public CreateVacancy salary(String salaryF,
                                String salaryT){
        $(salaryFrom).setValue(salaryF);
        $(salaryTo).setValue(salaryT);
        return page(CreateVacancy.class);
    }
    public CreateVacancy experience(){
        $(experienceEl).click();
        $(getExperienceOneYear).click();
        return page(CreateVacancy.class);
    }

    public CreateVacancy specialization () {
        $(specializashionEl).waitUntil(Condition.visible, 2000).click();
        $(specialCheckBox).click();
        $(specialSave).click();
        return page(CreateVacancy.class);
    }

    public CreateVacancy description(){
        By frame = By.xpath("//tr/td[../td/text()='Описание вакансии']//iframe");
        $(frame).scrollTo();
        switchTo().frame($(frame));
        By inFrame = By.xpath("//body");
        System.out.println($(inFrame).innerHtml());
        $(inFrame).sendKeys("aaaaaaaaaaa");
        sleep(2000);
        switchTo().defaultContent();
        return page(CreateVacancy.class);
    }

    public CreateVacancy shedule(){
        $(shedulesPolniyDen).scrollTo().click();
        return page(CreateVacancy.class);
    }

    public void workPlace(String address){
        addPlaceWork.click();
     //   $(addPlaceWorkAddress).setValue(address).waitUntil(Condition.exist,1000)
     //           .sendKeys(Keys.DOWN);
        addPlaceWorkRegion.click();
        workRegionMSK.click();
        workPlaceDoneButton.click();
        addPlaceCreateButton.click();
    }

    public CreateVacancy rubric(){
        $(rubricEl).scrollTo().click();
        return page(CreateVacancy.class);
    }

    public CreateVacancy saveVacancy(){
        $(save).scrollTo().click();
        return page(CreateVacancy.class);
    }

    public String titleError(){ return($(titleError).getText());  }
    public String experienceError(){ return ($(experienceError).getText());}
    public String descriptionError(){ return ($(descriptionError).getText());}
    public String sheduleError(){ return ($(sheduleError).getText());}
    public String rubrikError(){ return ($(rubrikError).getText());}
    public String salaryFromError(){ return ($(salaryFromError).getText());}
//    public String salaryFromError2(){return ($(SalaryFromError2).waitUntil(visible,4000).getText());}
    public String salaryFromError2(){return ($(SalaryFromError2).shouldHave(Condition.exist).getText());}

    public void succesCreateVacancy(String title,
                                    String salaryFrom,
                                    String salaryTo){
        hrPage = new pages.employer.HrPage();
        hrPage.clickOnCreateVacancy();
        titleVacancy(title);
        salary(salaryFrom, salaryTo);
        experience();
        specialization();
        description();
        shedule();
        saveVacancy();
    }


}
