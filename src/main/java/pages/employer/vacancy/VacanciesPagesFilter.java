package pages.employer.vacancy;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class VacanciesPagesFilter {

    //кнопки и поля
    private SelenideElement publish                 = $x("//div[@class='tabs vacancies-tabs vacancies__tabs']//div[2]//a[1]");
    private SelenideElement unpublish               = $x("//div[3]//a[1]//span[1]");
    private SelenideElement archive                 = $x("//div[4]//a[1]//span[1]");

    //фильтры
    private SelenideElement filter                  = $x("//div[@class='collapse-block vacancies-filter vacancies__filters collapse-block_collapsed collapse-block_left-arrow collapse-block_theme-light']//div[@class='collapse-block__collapsing-title']");
    private SelenideElement searchTitle             = $x("//div[@class='card__text vacancies-filter__fields']/div[1]");
    private SelenideElement searchRecruiter         = $x("//div[@class='card__text vacancies-filter__fields']/div[2]");
    private SelenideElement selectRecruiter         = $x("//div[@class='menu__content menu__content_select hr-select__content menuable__content__active']//div[@class='list__tile__sub-title']");
    private SelenideElement searchContactPerson     = $x("//div[@class='card__text vacancies-filter__fields']/div[3]");
    private SelenideElement selectContactPerson     = $x ("//div[@class='menu__content menu__content_select hr-select__content menuable__content__active']//div[@class='list__tile__sub-title']");
    private SelenideElement searchCity              = $x("//div[@class='card__text vacancies-filter__fields']/div[4]");
    private SelenideElement selectCity              =$x("//div[@class='menu__content menu__content_select hr-select__content menuable__content__active']//a[@class='list__tile list__tile_link']");
    private SelenideElement clearButton             = $x("//button[@aria-label='Очистить']");
    private SelenideElement showButton              = $x("//button[@aria-label='Показать']");
    private SelenideElement clickVneFiltra          = $x("//div[@class='card__actions vacancies-filter__actions']");
    private SelenideElement chooseAll               = $x("//div[@class='hr-checkbox vacancies__select-all input-group input-group_label input-group_static-label checkbox input-group_selection-controls text_primary']//span[@class='checkbox__content']");


    public VacanciesPagesFilter clickOnPublish(){
        publish.click();
        return page(VacanciesPagesFilter.class);
    }
    public VacanciesPagesFilter clickOnUnpublish(){
        unpublish.click();
        return page(VacanciesPagesFilter.class);
    }
    public VacanciesPagesFilter clickOnArchived(){
        archive.click();
        return page(VacanciesPagesFilter.class);
    }
    public VacanciesPagesFilter clickOnFilter(){
        filter.click();
        return page(VacanciesPagesFilter.class);
    }
    public VacanciesPagesFilter clickOnSearchTitle(){
        searchTitle.click();
        return page(VacanciesPagesFilter.class);
    }
    public VacanciesPagesFilter clickOnSearchRecruiter(){
        searchRecruiter.click();
        selectRecruiter.shouldHave(Condition.exist).click();
       // sleep(100);
        clickVneFiltra.click();
        return page(VacanciesPagesFilter.class);
    }
    public VacanciesPagesFilter clickOnSearchContactPerson(){
        searchContactPerson.click();
        selectContactPerson.shouldHave(Condition.exist).click();
       // sleep(200);
        clickVneFiltra.click();
        return page(VacanciesPagesFilter.class);
    }
    public VacanciesPagesFilter clickOnSearchCity(){
        searchCity.click();
        selectCity.shouldHave(Condition.exist).click();
        //sleep(200);
        clickVneFiltra.click();
        return page(VacanciesPagesFilter.class);
    }

    public void showButtonFilter(){
        showButton.click();
    }
    public void clearButtonFilter(){
        clearButton.click();
    }
    public String textCleanCity(){
        return searchCity.getText();
    }

}
