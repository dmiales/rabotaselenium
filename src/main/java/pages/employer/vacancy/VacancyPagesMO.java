package pages.employer.vacancy;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class VacancyPagesMO {

    public SelenideElement publicVacancy                  =$x("//button[@aria-label='Разместить']");
    public SelenideElement copyVacancy                    =$x("//button[@aria-label='Копировать']");
    public SelenideElement deleteVacancy                  =$x("//button[@aria-label='Удадить']");
    public SelenideElement editVacancy                    =$x("//button[@aria-label='Редактировать']");
    public SelenideElement archiveVacancy                 =$x("//button[@aria-label='В архив']");

    public SelenideElement selectVacancy                  =$x("//div[@class='hr-checkbox input-group input-group_dirty checkbox input-group_selection-controls input-group_active text_primary']//span[@class='checkbox__content']");

    public VacancyPagesMO publicVacancyMO(){
        publicVacancy.click();
        return page(VacancyPagesMO.class);
    }
    public VacancyPagesMO copyVacancyMO(){
        copyVacancy.click();
        return page(VacancyPagesMO.class);

    }
    public VacancyPagesMO deleteVacancyMO(){
        deleteVacancy.click();
        return page(VacancyPagesMO.class);

    }
    public VacancyPagesMO editVacancyMO(){
        editVacancy.click();
        return page(VacancyPagesMO.class);

    }
    public VacancyPagesMO archiveVacancyMO(){
        archiveVacancy.click();
        return page(VacancyPagesMO.class);

    }

    //TODO поменять икспасс
    public VacancyPagesMO selectOneVacancy(){
        selectVacancy.scrollTo().click();
        return page(VacancyPagesMO.class);

    }
}
