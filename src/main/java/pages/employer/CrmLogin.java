package pages.employer;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class CrmLogin {

    private SelenideElement loginCRM                   =$x("//input[@id='username']");
    private SelenideElement passwordCRM                =$x("//input[@id='password']");
    private SelenideElement signIn                  = $x(" //input[@class='btn btn-lg btn-primary btn-block']");


    public void setLoginCRM(String login){
        loginCRM.setValue(login);
    }
    public void setPasswordCRM(String password){
        passwordCRM.setValue(password);
    }
    public void clickSignInButton(){
        signIn.click();
    }
}
