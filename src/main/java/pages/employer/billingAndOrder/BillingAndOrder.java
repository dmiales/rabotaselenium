package pages.employer.billingAndOrder;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class BillingAndOrder {

    private SelenideElement numberOrderField                                 =$x("(//div[@class='lblock_info_block_title fleft']/a[@class='title_link large'])[1]");



    public String number;

    public void getNumberOrder(){
        String[] parts = numberOrderField.getText().split("-");
        number = parts[parts .length-1];
    }


}
