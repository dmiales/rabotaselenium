package pages.worker.favorites;

import org.openqa.selenium.By;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import utils.Preparation;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class FavoritesVacanciesPage {

    public void screenShot() throws Exception {
        Preparation prep = new Preparation();

        Thread.sleep(2000);
        Screenshot fpScreenshot = new AShot()
                .shootingStrategy(ShootingStrategies.viewportPasting(3000))
                .addIgnoredElement(By.className("main-cols__cls"))
                .addIgnoredElement(By.className("sf-toolbarreset clear-fix"))
                .takeScreenshot(getWebDriver());
        prep.screenShot(fpScreenshot, "FavoritesVacanciesPage");
    }
}
