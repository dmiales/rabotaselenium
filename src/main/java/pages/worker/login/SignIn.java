package pages.worker.login;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import utils.Preparation;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class SignIn {

    final String loginRecruiter = "ndk4@kama.rdw.ru";
    final String login = "dmiales3@gmail.com";
    //final String login = "dmial123123123123123123123es@gmail.com";

    final String correctPassword = "123456";
    //final String correctPassword = "4fc0d28c";
    final String wrongPassword = "1234561";
    private final By loginInput = By.xpath("//div/div[../label/span/text()='E-mail или мобильный телефон']/div/div/input");
    private final By passwordInput = By.xpath("//div/div[../label/span/text()='Пароль']/div/div/input");
    private final By signInButton = By.xpath("//button[starts-with(normalize-space(text()),'Войти')]");
    private final By wrongAlert = By.xpath("//span/div/div/div[starts-with(normalize-space(text()),'Неверный логин или пароль')]");
    private final By registerPage = By.xpath("//a[text()='как соискатель']");

    public void getRegisterPage() {
        $(registerPage).should(exist).click();
    }

    private void typeLogin(String login) {
        $(loginInput).should(exist).sendKeys(Keys.CONTROL + "a");
        $(loginInput).sendKeys(Keys.DELETE);
        $(loginInput).sendKeys(login);
    }

    private void typePassword(String password) {
        $(passwordInput).should(exist).sendKeys(Keys.CONTROL + "a");
        $(passwordInput).sendKeys(Keys.DELETE);
        $(passwordInput).sendKeys(password);
    }

    public void screenShot() throws Exception {
        Preparation prep = new Preparation();

        Thread.sleep(2000);
        Screenshot fpScreenshot = new AShot()
                .shootingStrategy(ShootingStrategies.viewportPasting(1000))
                .takeScreenshot(getWebDriver());
        prep.screenShot(fpScreenshot, "LoginPage");
    }

    public void signInSuccess() {
        this.typeLogin(login);
        this.typePassword(correctPassword);
        $(signInButton).should(exist).click();
    }

    public void signInFail() {
        this.typeLogin(login);
        this.typePassword(wrongPassword);
        $(signInButton).should(exist).click();

        $(wrongAlert).should(exist);
//        (new WebDriverWait(driver, 2)).until(ExpectedConditions.visibilityOfElementLocated(wrongAlert));
//
//        System.out.println(driver.findElement(wrongAlert).getSize().height==24);
//        System.out.println(driver.findElement(wrongAlert).getSize().width==198);
    }
}
