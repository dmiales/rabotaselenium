package pages.worker.login;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import utils.Preparation;

import java.sql.Timestamp;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class Register {

    final String fi = "Самохвалов Дмитрий";
    final String birthday = "1991";
    final String profession = "Водитель";
    final String login = "dmiales@gmail.com";


    private final By fiInput = By.xpath("//div/div[../label/span/text()='Имя и фамилия']/div/div/input");
    private final By birthdayInput = By.xpath("//div/div[../label/span/text()='Год рождения']/div/div/input");
    private final By professionInput = By.xpath("//div/div[../label/span/text()='Желаемая должность']/div/div/div/div/div/input");
    private final By loginInput = By.xpath("//div/div[../label/span/text()='E-mail или мобильный телефон']/div/div/input");
    private final By nextStep = By.xpath("//button[starts-with(normalize-space(text()),'Продолжить')]");
    private final By firstProfession = By.xpath("//ul[@class='list']/li[position() = 1]");

    private void fillInFi(String fi) {
        $(fiInput).should(exist).sendKeys(Keys.CONTROL + "a");
        $(fiInput).sendKeys(Keys.DELETE);
        $(fiInput).sendKeys(fi);
    }

    private void fillInBirthday(String birthday) {
        $(birthdayInput).should(exist).sendKeys(Keys.CONTROL + "a");
        $(birthdayInput).sendKeys(Keys.DELETE);
        $(birthdayInput).sendKeys(birthday);
    }

    private void fillInProfession(String profession) {
        $(professionInput).should(exist).sendKeys(Keys.CONTROL + "a");
        $(professionInput).sendKeys(Keys.DELETE);
        $(professionInput).sendKeys(profession);
        $(firstProfession).should(appear).click();
    }

    private void fillInLogin(String login) {
        $(loginInput).should(exist).sendKeys(Keys.CONTROL + "a");
        $(loginInput).sendKeys(Keys.DELETE);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        login = timestamp.getTime() + login;
        $(loginInput).sendKeys(login);
    }

    public void screenShot() throws Exception {
        Preparation prep = new Preparation();

        Thread.sleep(2000);
        Screenshot fpScreenshot = new AShot()
                .shootingStrategy(ShootingStrategies.viewportPasting(1000))
                .takeScreenshot(getWebDriver());
        prep.screenShot(fpScreenshot, "RegisterPage");
    }

    public void register() {
        this.fillInFi(fi);
        this.fillInBirthday(birthday);
        this.fillInProfession(profession);
        this.fillInLogin(login);
        $(nextStep).should(exist).click();
    }
}
