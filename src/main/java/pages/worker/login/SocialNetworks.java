package pages.worker.login;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;

public class SocialNetworks {

    public By vkSocialButton = By.xpath("//a[@class=\"r-btn r-btn_icon r-btn_medium social-button social-button_original-icon social-button_type_vk\"]");
    public By fbSocialButton = By.xpath("//a[@class=\"r-btn r-btn_icon r-btn_medium social-button social-button_original-icon social-button_type_fb\"]");
    public By okSocialButton = By.xpath("//a[@class=\"r-btn r-btn_icon r-btn_medium social-button social-button_original-icon social-button_type_ok\"]");
    public By yandexSocialButton = By.xpath("//a[@class=\"r-btn r-btn_icon r-btn_medium social-button social-button_original-icon social-button_type_yandex\"]");
    public By mailSocialButton = By.xpath("//a[@class=\"r-btn r-btn_icon r-btn_medium social-button social-button_original-icon social-button_type_mail\"]");
    public By googleSocialButton = By.xpath("//a[@class=\"r-btn r-btn_icon r-btn_medium social-button social-button_original-icon social-button_type_google\"]");

    //  id пользователей для различных соцсетей
    public String vkUserId = "7748505";

    public String fbUserId = "19414718";
    //    Наташа Воробьева
    public String okUserId = "9898192";
    //    Алан Кодзаев
    public String yandexUserId = "19008352";
    //    Евгений Саламонов
    public String mailUserId = "17451111";
    //    Евгений Полинов
    public String googleUserId = "20227103";
    //    Вячеслав Смирницкий

    private final By userLink = By.xpath("//a[text()='Существующий пользователь']");
    private final By fieldId = By.xpath("//input[@id='ele-oauth_exists_user_form-user']");
    private final By button = By.xpath("//div[@id='exists']//button");

    private final By userName = By.xpath("//div[@class='user-profile-menu__title']");


    public void clickSocialButton(By socialButton) {
        $(socialButton).should(exist).click();
        //return new SocialNetworks(driver);
    }

    public void mockAuthorization(String userId) {
        $(userLink).should(exist).click();
        $(fieldId).should(exist).sendKeys(userId);
        $(button).should(exist).click();
        //return new SocialNetworks(driver);
    }

    public String getUserName() {
        return $(userName).should(exist).getText();
    }
}
