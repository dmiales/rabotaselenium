package pages.worker.resume;

import org.openqa.selenium.By;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import utils.Preparation;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class ResumePage {

    private final By saveResume = By.xpath("//button[@class='register-form__submit r-btn r-btn_large secondary'][starts-with(normalize-space(text()),'Сохранить')]");
    /*Блоки резюме*/
    private final By h1ResumeAddition = By.xpath("//h1/span[text()='Дополнение резюме']");
    private final By h2WishesToWork = By.xpath("//h2[text()='Пожелания к работе']");
    private final By h2WorkExperience = By.xpath("//h2[text()='Опыт работы']");
    private final By h2Education = By.xpath("//h2[text()='Образование']");
    private final By h2SpecifyOptional = By.xpath("//h2[text()='Указать дополнительно']");

    public void saveResume() {
        System.out.println("Сохраняем резюме");
        $(saveResume).should(exist).click();
        System.out.println("Сохранили резюме");
    }

    public void screenShot() throws Exception {
        Preparation prep = new Preparation();

        Thread.sleep(2000);
        Screenshot fpScreenshot = new AShot()
                .shootingStrategy(ShootingStrategies.viewportPasting(100))
                .takeScreenshot(getWebDriver());
        prep.screenShot(fpScreenshot, "ResumePage");
    }

    public void screenShotFull() throws Exception {
        Preparation prep = new Preparation();

        Thread.sleep(2000);
        Screenshot fpScreenshot = new AShot()
                .shootingStrategy(ShootingStrategies.viewportPasting(100))
                .takeScreenshot(getWebDriver());
        prep.screenShot(fpScreenshot, "ResumePageFull");
    }

}
