package pages.worker.resume.Sections;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import java.awt.*;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class MainSection {

    final String phoneNumber = "89163876711";

    private final By avatarFile = By.xpath("//div[starts-with(normalize-space(text()),'Загрузить')]");
    private final By loadAvatarFile = By.xpath("//div[div/nav/div/div[starts-with(normalize-space(text()),'Редактирование фото')]]/div/div/div/input");
    private final By saveAvatarFile = By.xpath("//div[div/nav/div/div[starts-with(normalize-space(text()),'Редактирование фото')]]/div/div/div/button[starts-with(normalize-space(text()),'Сохранить')]");
    private final By gender = By.xpath("//button[starts-with(normalize-space(text()),'Мужчина')]"); //Женщина
    private final By citizenship = By.xpath("//div/div[../label/span/text()='Гражданство']/div/div/div/div/div/input");
    private final By firstCitizenship = By.xpath("//ul[@class='list']/li[position() = 1]/a/div/div[text()='Россия']"); //выбрать другую страну
    private final By city = By.xpath("//div/div[../label/span/text()='Город проживания']/div/div/div/div/input");
    private final By firstCity = By.xpath("//ul[@class='list']/li[position() = 1]/a/div/div[text()='Москва']");
    private final By metro = By.xpath("//div/div[../label/span/text()='Ближайшее метро']/div/div/div/div/input");
    private final By firstMetro = By.xpath("//ul[@class='list']/li[position() = 1]/a/div/div/span[text()='Планерная']");
    private final By phone = By.xpath("//div/div[../label/span/text()='Мобильный телефон']/div/div/div/input");
    private final By isMarried = By.xpath("//div[../div/label/text()='Состою в браке']/div");

    private final By haveChildren = By.xpath("//div[../div/label/text()='Есть дети']/div");

    private void fillAvatarFile() throws AWTException, InterruptedException {
        JavascriptExecutor js = ((JavascriptExecutor) getWebDriver());
        String photoImage = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==";
        js.executeScript("__NUXT__.state.rabota.resume.croppedPhoto = \"" + photoImage + "\"");
        System.out.println("Ставлю avatarFile");
    }

    private void fillGender() {
        System.out.println("Ставлю gender");
        $(gender).should(exist).click();
    }

    private void fillCitizenship() {
        System.out.println("Ставлю citizenship");
        $(citizenship).should(exist).click();
        $(firstCitizenship).should(exist).click();
    }

    private void fillCity() {
        System.out.println("Ставлю city");
        $(city).should(exist).click();
        $(firstCity).should(exist).click();
    }

    private void fillMetro() {
        System.out.println("Ставлю metro");
        $(metro).should(exist).click();
        $(firstMetro).should(exist).click();
    }

    private void fillPhone(String phoneNumber) {
        System.out.println("Ставлю phone");
        $(phone).should(exist).click();
        $(phone).sendKeys(phoneNumber);
    }

    private void fillIsMarried() {
        System.out.println("Ставлю isMarried");
        $(isMarried).should(exist).click();
    }

    private void fillHaveChildren() {
        System.out.println("Ставлю haveChildren");
        $(haveChildren).should(exist).click();
    }

    public void fillMainSection() throws AWTException, InterruptedException {
        this.fillAvatarFile();
        this.fillGender();
        this.fillCitizenship();
        this.fillCity();
        this.fillMetro();
        this.fillPhone(phoneNumber);
        this.fillIsMarried();
        this.fillHaveChildren();
    }

}
