package pages.worker.resume.Sections;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;

public class ExperienceSection {

    final String company = "Company ASD";
    final String position = "Дезинфектор";
    final String yearOfWorkNumber = "1991";
    final String achievementsText = "achievementsText achievementsText achievementsText achievementsText";

    private final By isUnemployed = By.xpath("//div[../div/label/text()='У меня нет опыта работы']/div/span");
    private final By cityOfWork = By.xpath("//div/div[../label/span/text()='Город']/div/div/div/div/input");
    private final By firstCityOfWork = By.xpath("//ul[@class='list']/li[position() = 1]/a/div/div[text()='Москва']");
    private final By companyOfWork = By.xpath("//div/div[../label/span/text()='Компания']//input");
    private final By positionOfWork = By.xpath("//div/div[../label/span/text()='Должность']//input");
    private final By firstPositionOfWork = By.xpath("//ul[@class='list']/li[position() = 1]/a/div/div/span[text()='дезинфектор']");
    private final By yearOfWork = By.xpath("//div/div[../label/span/text()='Начало работы']/div/div/div/div/input");
    private final By monthOfWork = By.xpath("(//div/div[../label/span/text()='Начало работы']/div/div/div/div/div)[1]");
    private final By firstMonthOfWork = By.xpath("(//div[../div/div/text()='Февраль'])[2]");
    private final By isUntilNow = By.xpath("//div[../div/label/text()='По настоящее время']/div");
    private final By achievements = By.xpath("//div/div[../label/span/text()='Функции и достижения']//textarea");

    private ExperienceSection fillCityOfWork() {
        System.out.println("Ставлю cityOfWork");
        $(cityOfWork).should(exist).click();
        System.out.println("2 cityOfWork");
        $(firstCityOfWork).should(exist).click();
        return this;
    }

    private ExperienceSection fillCompanyOfWork(String company) {
        System.out.println("Ставлю companyOfWork");
        $(companyOfWork).should(exist).click();
        $(companyOfWork).sendKeys(company);
        return this;
    }

    private ExperienceSection fillPositionOfWork(String position) {
        System.out.println("Ставлю PositionOfWork");
        $(positionOfWork).should(exist).sendKeys(position);
        //System.out.println("Ставлю PositionOfWork2");

        //(new WebDriverWait(driver, 5)).until(ExpectedConditions.elementToBeClickable(positionOfWork));
        //System.out.println("Нашли PositionOfWork");
        //WebElement positionOfWorkEl = driver.findElement(positionOfWork);
        //positionOfWorkEl.click();
        $(firstPositionOfWork).should(exist).click();
        return this;
    }

    private ExperienceSection fillYearOfWork(String yearOfWorkNumber) {
        System.out.println("Ставлю yearOfWork");
        $(yearOfWork).should(exist).click();
        $(yearOfWork).sendKeys(yearOfWorkNumber);
        return this;
    }

    private ExperienceSection fillMonthOfWork() {
        System.out.println("Ставлю monthOfWork");
        $(monthOfWork).should(exist).click();
        System.out.println("2 monthOfWork");
        $(firstMonthOfWork).should(exist).click();
        return this;
    }

    private ExperienceSection fillIsUntilNow() {
        System.out.println("Ставлю isUntilNow");
        $(isUntilNow).should(exist).click();
        return this;
    }

    private ExperienceSection fillAchievements(String achievementsText) {
        System.out.println("Ставлю achievements");
        $(achievements).should(exist).click();
        $(achievements).sendKeys(achievementsText);
        return this;
    }

    public void fillExperienceSection() {
        this.fillCityOfWork();
        this.fillCompanyOfWork(company);
        this.fillPositionOfWork(position);
        this.fillYearOfWork(yearOfWorkNumber);
        this.fillMonthOfWork();
        this.fillIsUntilNow();
        this.fillAchievements(achievementsText);
    }
}
