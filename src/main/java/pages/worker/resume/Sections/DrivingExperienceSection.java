package pages.worker.resume.Sections;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;

public class DrivingExperienceSection {

    private final By drivingExperience = By.xpath("//span[text()='Опыт вождения']");
    private final By isCategoryA = By.xpath("//div[../div/label/text()='A']/div");
    private final By isCategoryB = By.xpath("//div[../div/label/text()='B']/div");
    private final By isCategoryC = By.xpath("//div[../div/label/text()='C']/div");
    private final By isCategoryD = By.xpath("//div[../div/label/text()='D']/div");
    private final By isCategoryE = By.xpath("//div[../div/label/text()='E']/div");
    private final By haveCar = By.xpath("//div[../div/label/text()='Есть свой автомобиль']/div/div");

    private DrivingExperienceSection fillDrivingExperience() {
        System.out.println("Ставлю drivingExperience");
        $(drivingExperience).should(exist).click();
        return this;
    }

    private DrivingExperienceSection fillIsCategoryA() {
        System.out.println("Ставлю drivingExperience");
        $(isCategoryA).should(exist).click();
        return this;
    }

    private DrivingExperienceSection fillIsCategoryB() {
        System.out.println("Ставлю drivingExperience");
        $(isCategoryB).should(exist).click();
        return this;
    }

    private DrivingExperienceSection fillIsCategoryC() {
        System.out.println("Ставлю drivingExperience");
        $(isCategoryC).should(exist).click();
        return this;
    }

    private DrivingExperienceSection fillIsCategoryD() {
        System.out.println("Ставлю drivingExperience");
        $(isCategoryD).should(exist).click();
        return this;
    }

    private DrivingExperienceSection fillIsCategoryE() {
        System.out.println("Ставлю drivingExperience");
        $(isCategoryE).should(exist).click();
        return this;
    }

    private DrivingExperienceSection fillHaveCar() {
        System.out.println("Ставлю drivingExperience");
        $(haveCar).should(exist).click();
        return this;
    }

    public void fillDrivingExperienceSection() {
        this.fillDrivingExperience();
        this.fillIsCategoryA();
        this.fillIsCategoryB();
        this.fillIsCategoryC();
        this.fillIsCategoryD();
        this.fillIsCategoryE();
        this.fillHaveCar();
    }
}
