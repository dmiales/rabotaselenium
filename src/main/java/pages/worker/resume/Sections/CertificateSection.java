package pages.worker.resume.Sections;

import org.openqa.selenium.By;

public class CertificateSection {

    private final By certificates = By.xpath("//span[text()='Диплом или сертификат']");
    private final By certificateName = By.xpath("//div/div[../label/span/text()='Название']//input");
    private final By certificateYear = By.xpath("//div/div[../label/span/text()='Год получения']//input");
    private final By certificateFile = By.xpath("//div[starts-with(normalize-space(text()),'Выбрать файл...')]");
}
