package pages.worker.resume.Sections;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;

public class LanguageSection {

    private final By languages = By.xpath("//span[text()='Владение языками']");
    private final By nationalLanguage = By.xpath("//div/div[../label/span/text()='Родной язык']//input");
    private final By firstNationalLanguage = By.xpath("//ul[@class='list']/li[position() = 1]/a/div/div[text()='Русский']");
    private final By foreignLanguage = By.xpath("//div/div[../label/span/text()='Язык']//input");
    private final By firstForeignLanguage = By.xpath("//ul[@class='list']/li[position() = 1]/a/div/div[text()='Английский']");
    private final By levelLanguage = By.xpath("(//div/div[../label/span/text()='Уровень владения']/div/div/div/div)[1]");
    private final By firstLevelLanguage = By.xpath("//ul[@class='list']/li[position() = 1]/a/div/div[text()='начальный']");

    private LanguageSection fillLanguages() {
        System.out.println("Ставлю languages");
        $(languages).should(exist).click();
        return this;
    }

    private LanguageSection fillNationalLanguage() {
        System.out.println("Ставлю nationalLanguage");
        $(nationalLanguage).should(exist).click();
        System.out.println("2 firstProfessionalSkills");
        $(firstNationalLanguage).should(exist).click();
        return this;
    }

    private LanguageSection fillForeignLanguage() {
        System.out.println("Ставлю foreignLanguage");
        $(foreignLanguage).should(exist).click();
        System.out.println("2 firstForeignLanguage");
        $(firstForeignLanguage).should(exist).click();
        return this;
    }

    private LanguageSection fillLevelLanguage() {
        System.out.println("Ставлю levelLanguage");
        $(levelLanguage).should(exist).click();
        System.out.println("2 firstLevelLanguage");
        $(firstLevelLanguage).should(exist).click();
        return this;
    }

    public void fillLanguageSection() {
        this.fillLanguages();
        this.fillNationalLanguage();
        this.fillForeignLanguage();
        this.fillLevelLanguage();
    }
}
