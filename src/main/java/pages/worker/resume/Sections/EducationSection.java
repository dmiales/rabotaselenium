package pages.worker.resume.Sections;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;

public class EducationSection {

    final String educationalInstitutionText = "EducationalInstitutionText";
    final String yearOfEducationalText = "1992";
    final String specialtyOfEducationalText = "specialtyOfEducationalText";

    private final By levelOfEducation = By.xpath("(//div/div[../label/span/text()='Уровень образования']/div/div/div)[1]");
    private final By firstLevelOfEducation = By.xpath("//ul[@class='list']/li[position() = 3]/a/div/div[text()='неполное высшее']");
    private final By educationalInstitution = By.xpath("//div/div[../label/span/text()='Учебное заведение']//input");
    private final By yearOfEducational = By.xpath("//div/div[../label/span/text()='Год выпуска']//input");
    private final By specialtyOfEducational = By.xpath("//div/div[../label/span/text()='Специальность']//input");

    private EducationSection fillLevelOfEducation() {
        System.out.println("Ставлю levelOfEducation");
        $(levelOfEducation).should(exist).click();
        $(firstLevelOfEducation).should(exist).click();
        return this;
    }

    private EducationSection fillEducationalInstitution(String educationalInstitutionText) {
        System.out.println("Ставлю educationalInstitution");
        $(educationalInstitution).should(exist).click();
        $(educationalInstitution).sendKeys(educationalInstitutionText);
        return this;
    }

    private EducationSection fillYearOfEducational(String yearOfEducationalText) {
        System.out.println("Ставлю yearOfEducational");
        $(yearOfEducational).should(exist).click();
        $(yearOfEducational).sendKeys(yearOfEducationalText);
        return this;
    }

    private EducationSection fillSpecialtyOfEducational(String specialtyOfEducationalText) {
        System.out.println("Ставлю specialtyOfEducational");
        $(specialtyOfEducational).should(exist).click();
        $(specialtyOfEducational).sendKeys(specialtyOfEducationalText);
        return this;
    }

    public void fillEducationSection() {
        this.fillLevelOfEducation();
        this.fillEducationalInstitution(educationalInstitutionText);
        this.fillYearOfEducational(yearOfEducationalText);
        this.fillSpecialtyOfEducational(specialtyOfEducationalText);
    }

}
