package pages.worker.resume.Sections;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;

public class ProfessionalSkillsSection {

    private final By professionalSkillsButton = By.xpath("//span[text()='Профессиональные навыки']");
    private final By professionalSkills = By.xpath("//section[h2/text()='Профессиональные навыки']//input");
    private final By firstProfessionalSkills = By.xpath("//ul[@class='list']/li[position() = 1]/a/div/div[text()='уверенный пользователь ПК']");
    private final By newSkills = By.xpath("//section[./h2/text()='Профессиональные навыки']//input");

    private ProfessionalSkillsSection fillProfessionalSkillsButton() {
        System.out.println("Ставлю professionalSkillsButton");
        $(professionalSkillsButton).should(exist).click();
        return this;
    }

    private ProfessionalSkillsSection fillProfessionalSkills() {
        System.out.println("Ставлю professionalSkills");
        $(professionalSkills).should(exist).click();
        System.out.println("2 firstProfessionalSkills");
        $(firstProfessionalSkills).should(exist).click();
        return this;
    }

    public void fillProfessionalSkillsSection() {
        this.fillProfessionalSkillsButton();
        this.fillProfessionalSkills();
    }

}
