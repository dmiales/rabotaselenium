package pages.worker.resume.Sections;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;

public class WishesSection {

    final String salaryNumber = "30000";

    private final By salary = By.xpath("//div/div[../label/span/text()='Зарплата от, руб.']/div/div/input");
    private final By schedule = By.xpath("(//div/div[../label/span/text()='Предпочитаемый график']/div/div/div)[1]");
    private final By firstSchedule = By.xpath("//ul[@class='list']/li[position() = 1]/a/div/div[text()='полный рабочий день']");
    private final By isReadyForBusinessTrips = By.xpath("//div[../div/label/text()='Готов к командировкам']/div");
    private final By isReadyToMove = By.xpath("//div[../div/label/text()='Готов к переезду']/div");

    private WishesSection fillSalary(String salaryNumber) {
        System.out.println("Ставлю salary");
        $(salary).should(exist).click();
        $(salary).sendKeys(salaryNumber);
        return this;
    }

    private WishesSection fillSchedule() {
        System.out.println("Ставлю schedule");
        $(schedule).should(exist).click();
        $(firstSchedule).should(exist).click();
        return this;
    }

    private WishesSection fillIsReadyForBusinessTrips() {
        System.out.println("Ставлю isReadyForBusinessTrips");
        $(isReadyForBusinessTrips).should(exist).click();
        return this;
    }

    private WishesSection fillIsReadyToMove() {
        System.out.println("Ставлю isReadyToMove");
        $(isReadyToMove).should(exist).click();
        return this;
    }

    public void fillWishesSection() {
        this.fillSalary(salaryNumber);
        this.fillSchedule();
        this.fillIsReadyForBusinessTrips();
        this.fillIsReadyToMove();
    }
}

