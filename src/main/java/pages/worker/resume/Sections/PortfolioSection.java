package pages.worker.resume.Sections;

import org.openqa.selenium.By;

public class PortfolioSection {

    private final By portfolios = By.xpath("//span[text()='Портфолио']");
    private final By portfolioText = By.xpath("//div/div[../label/span/text()='Описание']//textarea");
    private final By portfolioFile = By.xpath("//div[starts-with(normalize-space(text()),'Выбрать файл...')]");
}
