package pages.worker.resume.Sections;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;

public class ExtraSection {

    final String aboutText = "aboutText aboutText aboutText aboutText aboutText";

    private final By about = By.xpath("//span[text()='О себе']");
    private final By aboutTextarea = By.xpath("//section[./h2/text()='О себе']//textarea");

    private ExtraSection fillAbout() {
        System.out.println("Ставлю drivingExperience");
        $(about).should(exist).click();
        return this;
    }

    private ExtraSection fillAboutText(String aboutText) {
        System.out.println("Ставлю aboutTextarea");
        $(aboutTextarea).should(exist).click();
        $(aboutTextarea).sendKeys(aboutText);
        return this;
    }

    public void fillExtraSection() {
        this.fillAbout();
        this.fillAboutText(aboutText);
    }
}
