package pages.worker;

import io.restassured.response.ExtractableResponse;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import utils.Preparation;


import java.io.IOException;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.url;
import static io.restassured.RestAssured.given;
import static org.apache.commons.lang3.StringUtils.trim;
import static org.testng.Assert.*;

public class VacancyPage {

    final String searchPosition = "Рабочий";

    private String Id;
    private String Title;
    private String Experience;
    private String PublishStartAt;
    private String CompanyId;
    private boolean IsFavorite;
    private String Schedule;
    private String Education;
    private String Salary;
    private String Description;
    private String ContactPersonName;
    private String ContactPersonNumberInternational;
    private String Company;
    private String Sid;

    //если залогинены
    private final By vacancyResponse = By.xpath("//div[@class='vacancy-actions vacancy-card__actions']//button[starts-with(normalize-space(text()),'Откликнуться')]");
    private final By vacancyResponse2 = By.xpath("//div[@class='vacancy-actions vacancy-card__actions vacancy-card__actions_faded']//button[starts-with(normalize-space(text()),'Откликнуться')]");
    private final By vacancyPhone = By.xpath("(//span[starts-with(normalize-space(text()),'Позвонить')])[1]");
    private final By vacancyPhoneContact = By.xpath("//div[@class='vacancy-phones__content-row'][1]");
    private final By vacancyPhonePhones = By.xpath("//div[@class='vacancy-phones__content-row']/a");

    private final By vacancyFavorite = By.xpath("//div[@class='vacancy-card__footer-actions']//button//span[starts-with(normalize-space(text()),'В избранное')]");
    private final By vacancyFavoriteLittle = By.xpath("//div[@class='vacancy-card__footer-actions']//button[contains(@aria-label,\"В избранное\")]");
    private final By vacancyMore = By.xpath("//div[@class='vacancy-card__footer-actions']//button//span[starts-with(normalize-space(text()),'Ещё')]");
    private final By vacancyBlackList = By.xpath("//ul[@class='list vacancy-more-list']//div[starts-with(normalize-space(text()),'В чёрный список')]");
    private final By vacancyComplain = By.xpath("//ul[@class='list vacancy-more-list']//div[starts-with(normalize-space(text()),'Пожаловаться')]");
    private final By vacancyH2Similar = By.xpath("//h2[text()='Похожие вакансии']");
    private final By vacancyH1 = By.xpath("//div[@class='vacancy-card__title']/h1");
    private final By vacancySchedule = By.xpath("//div[@class='info-table__line']/div[@itemprop='workHours']");
    private final By vacancyExperience = By.xpath("//div[@class='info-table__line']/div[@itemprop='experienceRequirements']");
    private final By vacancyEducation = By.xpath("//div[@class='info-table__line']/div[@itemprop='educationRequirements']");
    private final By vacancyDescription = By.xpath("//div[@class='vacancy-card__description']/div[@itemprop='description']");
    private final By vacancySalary = By.xpath("//div[@class='vacancy-card__salary']/h3");
    private final By vacancyCompany = By.xpath("//div[@class='vacancy-card__company-name']");

    private final By responseH1 = By.xpath("//div/h1/span");
    private final By responseAction = By.xpath("//button[starts-with(normalize-space(text()),'Откликнуться')]");
    private final By blackListAction = By.xpath("//button[starts-with(normalize-space(text()),'Добавить в чёрный список')]");
    private final By blackListAlert = By.xpath("//div[starts-with(normalize-space(text()),'Компания успешно добавлена')]");

    private final By complainReason = By.xpath("//div[@class='input-group__input']/div[2]");
    private final By complainAction = By.xpath("//button[starts-with(normalize-space(text()),'Отправить')]");
    private final By complainAlert = By.xpath("//div/div[text()='Жалоба успешно отправлена!']");


    //похожие вакансии
    private final By vacancySimilarResponse = By.xpath("//div[@class='vacancies-list']//button[2]/div[starts-with(normalize-space(text()),'Откликнуться')]");
    private final By vacancySimilarPhone = By.xpath("//div[@class='vacancies-list']//span[text()='Позвонить']");
    private final By vacancySimilarFavorite = By.xpath("//div[@class='vacancy-preview-actions__item']/button[div/*[name()='svg']]");
    private final By vacancySimilarExtend = By.xpath("//div[@class='vacancy-preview-actions__item hidden-sm-and-down']/div/button[div/*[name()='svg']][1]");
    private final By vacancySimilarMore = By.xpath("//div[@class='vacancy-preview-actions__item vacancy-preview-actions__item_more']//div/div/button[div/*[name()='svg']]");
    private final By vacancySimilarH3Title = By.xpath("//h3[@class='vacancy-preview-card__title-text']//a[@itemprop=\"title\"]");
    private final By vacancySimilarShortDescription = By.xpath("//div[@class='vacancy-preview-card__short-description']");
    private final By vacancySimilarFullDescription = By.xpath("//div[@class='vacancy-preview-card__full-description']");

    @Test
    public String getVacancyId() {
        $(vacancyH1).should(exist); //проверка, что мы на странице вакансии

        String url = url();
        return url.substring(url.indexOf("vacancy/") + 8, url.length() - 1);
    }

    @Test
    public void getApiVacancy() {

        this.Sid = getWebDriver().manage().getCookieNamed("sid").getValue();

        System.out.println(this.Sid);

        String vacancyId = getVacancyId();
        String api_v1_url = "https://api.titan.rabota.space/v4/";
        String payload = "{ \"request\": {" +
                "\"vacancy_ids\": [" + vacancyId + "], " +
                " \"fields\": [\"id\", \"title\", \"publish_start_at\",\"operating_schedule\", \"experience\" ,\"education\",\"breadcrumbs\",\"salary\",\"description\",\"contact_person\",\"company\",\"is_favourite\",\"company\"] " +
                "}" +
                "}";

        ExtractableResponse getVacancy = given()
                .request()
                .contentType("application/json")
                .baseUri(api_v1_url)
                .headers("Debug", "true")
                .headers("X-Token", this.Sid)
                .body(payload)
                .post("/vacancies.json")
                .then()
                .extract();
        System.out.println(payload);
        this.Id = getVacancy.path("response.vacancies[0].id") + "";
        this.PublishStartAt = getVacancy.path("response.vacancies[0].publish_start_at") + "";
        this.Company = getVacancy.path("response.vacancies[0].company.name") + "";
        this.IsFavorite = getVacancy.path("response.vacancies[0].is_favourite");
        this.CompanyId = getVacancy.path("response.vacancies[0].company.id") + "";

        String SalaryTo = getVacancy.path("response.vacancies[0].salary.to") + "";
        String SalaryFrom = getVacancy.path("response.vacancies[0].salary.from") + "";

        if (SalaryFrom.equals("null") && SalaryTo.equals("null")) {
            this.Salary = "договорная зарплата";
        } else if (SalaryFrom.equals("null")) {
            this.Salary = "до" + SalaryTo + "руб.";
        } else if (SalaryTo.equals("null")) {
            this.Salary = "от" + SalaryFrom + "руб.";
        } else {
            this.Salary = SalaryFrom + "—" + SalaryTo + "руб.";
        }

        this.Title = getVacancy.path("response.vacancies[0].title") + "";
        this.Experience = (getVacancy.path("response.vacancies[0].experience.name") + "").equals("null") ? "не имеет значения" : getVacancy.path("response.vacancies[0].experience.name") + "";
        this.Schedule = getVacancy.path("response.vacancies[0].operating_schedule.name") + "";
        this.Education = (getVacancy.path("response.vacancies[0].education.name") + "").equals("null") ? "любое" : getVacancy.path("response.vacancies[0].education.name") + "";
        this.Description = getVacancy.path("response.vacancies[0].description") + "";

        this.ContactPersonName = getVacancy.path("response.vacancies[0].contact_person.name") + "";

        System.out.println(this.ContactPersonName);
        System.out.println(this.Id);
        System.out.println(this.Title);
        System.out.println(this.Experience);
        System.out.println(this.Schedule);
        System.out.println(this.Education);
        //System.out.println(this.Description);


        payload = "{ \"request\": {" +
                "\"vacancy_id\": " + vacancyId + "}" +
                "}";

        ExtractableResponse getPhones = given()
                .request()
                .contentType("application/json")
                .baseUri(api_v1_url)
                .headers("X-Token", this.Sid)
                .body(payload)
                .post("/vacancy/phone.json")
                .then()
                .extract();
        System.out.println(payload);

        this.ContactPersonNumberInternational = getPhones.path("response.service_provider_phones[0].number_international") + "";

        System.out.println(this.ContactPersonNumberInternational);
    }

    @Test
    public void checkAllFields() {
        $(vacancyH1).should(exist).shouldHave(exactText(this.Title));
        $(vacancySchedule).should(exist).shouldHave(exactText(this.Schedule.toLowerCase()));
        $(vacancyExperience).should(exist).shouldHave(exactText(this.Experience.toLowerCase()));
        $(vacancyEducation).should(exist).shouldHave(exactText(this.Education.toLowerCase()));
        assertEquals(
                $(vacancyDescription).should(exist).innerHtml()
                        .replaceAll("\n", "")
                        .replaceAll("\r", ""),
                this.Description.replaceAll("(&laquo;)", "«")
                        .replaceAll("(&raquo;)", "»")
                        .replaceAll("(&ndash;)", "–")
                        .replaceAll("(<br />)", "<br>")
                        .replaceAll("\n", "")
                        .replaceAll("\r", "")
        );
        assertEquals(
                $(vacancySalary).should(exist).innerHtml().replaceAll("&nbsp;", "").replaceAll(" ", ""),
                this.Salary
        );
        $(vacancyCompany).should(exist).shouldHave(exactText(this.Company));
    }

    @Test
    public void checkSimilar() {
        JavascriptExecutor js = ((JavascriptExecutor) getWebDriver());
        System.out.println("Проверка похожих вакансий");
        js.executeScript("window.scrollTo(0,1000)");
        //$(vacancyH2Similar).should(exist); //проверка, что появился h2
        //$$(vacancySimilarH3Title).shouldHave(size(10));
    }

    @Test
    public void response() {
        if ($(vacancyResponse2).exists()) {
            $(vacancyResponse2).should(exist).click();
        } else {
            $(vacancyResponse).should(exist).click();
        }
        System.out.println("Нажали на кнопку откликнуться");
        $(responseH1).should(exist);
        $(responseAction).should(exist).click();
    }

    @Test
    public void phone() {
        System.out.println("проверка телефонов");
        $(vacancyPhone).should(exist).click();

        assertEquals(
                trim($(vacancyPhoneContact).should(exist).innerHtml()
                        .replaceAll("&nbsp;", "")
                        .replaceAll("\n", "")
                        .replaceAll("\r", "")),
                this.ContactPersonName
        );
        assertEquals(
                $$(vacancyPhonePhones).get(0).should(exist).text().replaceAll(" ", ""),
                this.ContactPersonNumberInternational.replaceAll("&nbsp;", "").replaceAll(" ", "")
        );
    }

    @Test
    public void favorite() {
        assertFalse(this.IsFavorite);
        if ($(vacancyFavorite).exists()) {
            $(vacancyFavorite).should(exist).click();
        } else {
            $(vacancyFavoriteLittle).should(exist).click();
        }
        getApiVacancy();
        assertTrue(this.IsFavorite);
    }

    @Test
    public void more() {
        $(vacancyMore).should(exist).click();
    }

    @Test
    public void blackList() {
        more();
        $(vacancyBlackList).should(exist).click();
        $(blackListAction).should(exist).click();
        $(blackListAlert).should(exist);

        String api_v1_url = "https://api.titan.rabota.space/v4/";
        String payload = "{ \"request\": {}}";
        System.out.println("Дошли до сюда3");
        ExtractableResponse getVacancy = given()
                .request()
                .contentType("application/json")
                .baseUri(api_v1_url)
                .headers("debug", "true")
                .headers("X-Token", this.Sid)
                .body(payload)
                .post("/me/companies/blacklist.json")
                .then()
                .extract();
        System.out.println("Дошли до сюда4");
        String blackListId = getVacancy.path("response.companies[0].id") + "";
        System.out.println(blackListId);
        System.out.println("Дошли до сюда5");
        assertEquals(blackListId, this.CompanyId);
    }

    public void screenShot() throws Exception {
        Preparation prep = new Preparation();
        try {
            prep.fixHead();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            fixResponse();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            hideSimilar();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        Thread.sleep(2000);
        Screenshot fpScreenshot = new AShot().coordsProvider(new WebDriverCoordsProvider())
                .shootingStrategy(ShootingStrategies.viewportPasting(100))
                .addIgnoredElement(By.className("page__aside"))
                .addIgnoredElement(By.className("vacancy-page__aside"))
                .addIgnoredElement(By.className("r-sticky-content"))
                .takeScreenshot(getWebDriver());
        prep.screenShot(fpScreenshot, "Vacancy");
    }

    @Test
    public void complain() {
        System.out.println("Дошли до сюда");
        more();
        $(vacancyComplain).should(exist).click();
        $(complainReason).should(exist).click();
        $(complainAction).should(exist).click();
        //
        // $(complainAlert).should(exist);
    }

    @Test
    public void hideSimilar() {
        JavascriptExecutor js = ((JavascriptExecutor) getWebDriver());
        js.executeScript("document.getElementsByClassName('vacancy-page__similar-list similar-vacancies-list')[0].style.display = \"none\";");
    }

    @Test
    public void fixResponse() {
        JavascriptExecutor js = ((JavascriptExecutor) getWebDriver());
        js.executeScript("document.getElementsByClassName('dialog__content')[0].style.position = \"static\";");
    }

    @Test
    public void checkAllFeatures() throws Exception {
        response();
        phone();
        favorite();
        blackList();
        complain();
        screenShot();
    }

}
