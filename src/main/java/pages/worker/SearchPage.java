package pages.worker;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class SearchPage {

    final String searchPosition = "Менеджер";
    final String cityTitle_spb = "Свежие вакансии в Санкт-Петербурге";
    final String cityTitle = "Свежие вакансии в Москве";
    final String cityProfessionTitle_spb = "Вакансии менеджера в Санкт-Петербурге";
    final String cityProfessionTitle = "Вакансии менеджера в Москве";

    private final By sandwich = By.xpath("//div[@class='user-profile-header__elements']");

    private final By position = By.xpath("//input[@placeholder='Должность или компания']");
    private final By searchWorkButton = By.xpath("//button[starts-with(normalize-space(text()),'Найти работу')]");
    private final By searchWorkButton_new = By.xpath("//button[starts-with(normalize-space(text()),'Найти')]");

    private final By specialization = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Специализация')]]/div/div/div/div/div/div/div[1]/label/a");
    private final By specializationCount = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Специализация')]]/div/div/div/div/div/div/div[1]/label/a/span[2]");

    private final By industries = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Отрасль компании')]]/div/div/div/div/div/div/div[1]/label/a");
    private final By industriesCount = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Отрасль компании')]]/div/div/div/div/div/div/div[1]/label/a/span[2]");

    private final By salary = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Зарплата')]]/div/div/div/div/div/div/div/div[1]/div/label/a");
    private final By salaryCount = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Зарплата')]]/div/div/div/div/div/div/div/div[1]/div/label/a/span[2]");

    private final By schedules = By.xpath("//div[div/div[starts-with(normalize-space(text()),'График работы')]]/div/div/div/div/div/div/div[1]/label/a");
    private final By schedulesCount = By.xpath("//div[div/div[starts-with(normalize-space(text()),'График работы')]]/div/div/div/div/div/div/div[1]/label/a/span[2]");

    private final By experience = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Требуемый опыт')]]/div/div/div/div/div/div/div[1]/label/a");
    private final By experienceCount = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Требуемый опыт')]]/div/div/div/div/div/div/div[1]/label/a/span[2]");

    private final By additional = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Показать вакансии')]]/div/div/div/div[1]/div/label/a");
    private final By additionalCount = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Показать вакансии')]]/div/div/div/div[1]/div/label/a/span[2]");

    private final String number = "1";

    private final By vacancyH3Title = By.xpath("(//h3[@class='vacancy-preview-card__title']//a[@itemprop=\"title\"])[" + number + "]");

    private final By sortForYou = By.xpath("//span[contains(text(),'Для вас')]");
    private final By sortNearly = By.xpath("//span[contains(text(),'Рядом')]");
    private final By sortNew = By.xpath("//span[contains(text(),'Новые')]");
    private final By sortSalary = By.xpath("//span[contains(text(),'По зарплате')]");

    private final By sortActionRel = By.xpath("//span[contains(text(),'Сортировать:')]");

    public void checkH1(String title) {
        By h1 = By.xpath("//h1[contains(text(),'" + title + "')]");
        System.out.println("Проверяем h1");
        $(h1).should(exist);
    }

    public void searchWork() {
        System.out.println("Кнопка поиска");
        $(searchWorkButton).should(exist).click();
    }

    public void setPosition(String searchPosition) {
        System.out.println("Выбираем профессию");
        $(position).should(exist).sendKeys(searchPosition);
        searchWork();
        searchWork();
        checkH1(cityProfessionTitle);
        sleep(2000);
    }

    public void setSpecialization() {
        System.out.println("Выбираем специализацию");
        String count = $(specializationCount).innerHtml();
        System.out.println(count);
        $(specialization).should(exist).click();
        checkVacancySearchUpdate(count);
    }

    public void setIndustries() {
        System.out.println("Выбираем Отрасль компании");
        String count = $(industriesCount).text();
        $(industries).should(exist).click();
        checkVacancySearchUpdate(count);
    }

    public void setSalary() {
        System.out.println("Выбираем зарлату");
        String count = $(salaryCount).text();
        $(salary).should(exist).click();

        By checkVacancyCount = By.xpath("//div[div/div[starts-with(normalize-space(text()),'Специализация')]]/div/div/div/div/div/div/div[1]/label/a/span[text()='" + count + "']");
        System.out.println($(checkVacancyCount).innerHtml());
        $(checkVacancyCount).should(exist);
    }

    public void setSchedules() {
        System.out.println("Выбираем график работы");
        String count = $(schedulesCount).text();
        $(schedules).should(exist).click();
        checkVacancySearchUpdate(count);
    }

    public void setExperience() {
        System.out.println("Выбираем опыт");
        String count = $(experienceCount).text();
        $(experience).should(exist).click();
        checkVacancySearchUpdate(count);
    }

    public void setAdditional() {
        System.out.println("Выбираем дополнительные настройки");
        String count = $(additionalCount).text();
        $(additional).should(exist).click();
        checkVacancySearchUpdate(count);
    }

    public void checkVacancySearchUpdate(String count) {
        System.out.println("Проверяем, что выдача обновилась");
        By checkVacancyCount = By.xpath("//a[span[text()='Любая']]/span[text()='" + count + "']");
        System.out.println(checkVacancyCount);
        $(checkVacancyCount).should(exist);
    }

    public void getSortForYou() {
        System.out.println("Смотрим выдачу Для вас");
        $(sortActionRel).should(exist).click(); //.click()
        $(sortForYou).should(exist).click(); //.click()
    }

    public void getSortNearly() {
        System.out.println("Смотрим выдачу Рядом");
        $(sortActionRel).should(exist).click();
        $(sortNearly).should(exist).click(); //.click()
    }

    public void getSortNew() {
        System.out.println("Смотрим выдачу Новые");
        $(sortActionRel).should(exist).click();
        $(sortNew).should(exist).click(); //.click()
    }

    public void getSortSalary() {
        System.out.println("Смотрим выдачу Новые");
        $(sortActionRel).should(exist).click();
        $(sortSalary).should(exist).click(); //.click()
    }

    public void getRandomVacancy() {
        System.out.println("Кликаем на рандомную вакансию");

        $(vacancyH3Title).should(exist).should(enabled).click();
    }

    public void fillSearchFilters() {
        setPosition(searchPosition);
        setSpecialization();
        setIndustries();
        setSchedules();
        setExperience();
        setAdditional();
        $(salary).should(exist).sendKeys(Keys.F5);
        setSalary();
    }

    public void checkAllSorts() {
        System.out.println("Проверка сортировок");
        Actions actions = new Actions(getWebDriver());

        actions.moveToElement($(sortActionRel)).perform();
        getSortNew();
        getSortForYou();
        getSortNearly();
        getSortSalary();
    }

}


//menu__content user-profile-menu__content menuable__content__active