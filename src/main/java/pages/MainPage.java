package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class MainPage {

    final String locationName = "Санкт Петербург Дворцовая площадь 2";

    private final By vacanciesPage = By.xpath("//button[contains(text(),'Показать ')]");
    private final By vacanciesPage_new = By.xpath("//button[starts-with(normalize-space(text()),'Найти')]");
    private final By signInButton = By.xpath("//button[starts-with(normalize-space(text()),'Войти')]");
    private final By locationSearchInput = By.xpath("//div[@class='input-group__input']/input[@placeholder='Адрес, метро или район']");
    private final By locationMapInput = By.xpath("//div[@class='location-picker__search-form']//div[@class='input-group__input']/input[@placeholder='Адрес, метро или район']");
    private final By locationMapName = By.xpath("//div[not(contains(@style,'display: none'))][not(contains(@style,'display:none'))]/div/ul[@class='list']/li[1]");
    private final By locationMapRadius = By.xpath("//div[@class='input-group__input']//div[text()='Любой радиус']");
    private final By locationMapRadius3km = By.xpath("//div[text()='3 км']");
    private final By locationMapSubmitButton = By.xpath("//button[@class='location-picker__search-form-submit r-btn r-btn_medium primary'][starts-with(normalize-space(text()),'Применить')]");
    private final By locationNewName = By.xpath("//div[contains(text(),'Поиск работы в Санкт-Петербурге')]");
    private final By h1Vacancy = By.xpath("//h1[contains(text(),'Работа и вакансии в Санкт-Петербурге')]");

    public void getVacanciesPage() {
        System.out.println("Получаем страницу вакансий");
        $(vacanciesPage).should(exist).click();
        WebDriverWait wait = new WebDriverWait(getWebDriver(), 10);
        wait.until(ExpectedConditions.urlContains("vacancy/"));
    }

    public void getLoginPage() {
        System.out.println("Получаем страницу логина");
        $(signInButton).should(exist).click();
        WebDriverWait wait = new WebDriverWait(getWebDriver(), 10);
        wait.until(ExpectedConditions.urlContains("passport/sign-in"));
    }

    public void getMap() {
        System.out.println("Получаем карту");
        $(locationSearchInput).should(exist).click();
        sleep(5000); //прогрузка карты
    }

    public void setRadius() {
        System.out.println("Ставим радиус");
        $(locationMapRadius).should(exist).click();
        $(locationMapRadius3km).should(exist).click();
    }

    public void setLocation(String locationName) {
        System.out.println("Ставим новый регион");
        $(locationMapInput).should(exist).click();
        $(locationMapInput).should(exist).sendKeys(Keys.CONTROL + "a");
        $(locationMapInput).sendKeys(Keys.DELETE);
        $(locationMapInput).sendKeys(locationName);
        $(locationMapName).should(exist).click();
    }

    public void submitLocation() throws InterruptedException {
        System.out.println("Применяем новый регион");
        Thread.sleep(2000);
        $(locationMapSubmitButton).shouldBe(enabled).click();
    }

    public void checkNewLocation() {
        System.out.println("Проверяем новый регион");
        //  $(locationNewName).should(exist);
        $(h1Vacancy).should(exist);
    }

    public void fillLocation() throws InterruptedException {
        this.getMap();
        this.setLocation(locationName);
        this.setRadius();
        this.submitLocation();
        this.getVacanciesPage();
        this.checkNewLocation();
    }

}