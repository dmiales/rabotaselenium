package pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;

public class Sandwich {

    private final By sandwich = By.xpath("//div[@class='user-profile-header__elements']");
    private final By sandwichActiveMenu = By.xpath("//div[@class='menu__activator menu__activator_active']");
    private final By resumes = By.xpath("//div/span[text()='Мои резюме']");
    private final By responses = By.xpath("//span[text()='Отклики']");
    private final By subscribes = By.xpath("//div[text()='Подписки']");
    private final By favoriteVacancies = By.xpath("//div[text()='Избранные вакансии']");
    private final By promo = By.xpath("//div[text()='Ускорить поиск работы']");
    private final By blackList = By.xpath("//div[text()='Чёрный список']");
    private final By profileSettings = By.xpath("//div[text()='Настройки профиля']");
    private final By exit = By.xpath("//div[text()='Выход']");

    public void sandwichCollapse() {
        System.out.println("Скрываем меню");
        $(sandwichActiveMenu).should(exist).click();
        $(sandwich).should(exist);
    }

    public void sandwichExpand() {
        System.out.println("Раскрываем меню");
        $(sandwich).should(exist).click();
        $(sandwichActiveMenu).should(exist);
    }

    public void resumes() {
        System.out.println("Переходим на страницу резюме");
        $(resumes).should(exist).click();
    }

    public void responses() {
        System.out.println("Переходим на страницу откликов");
        $(responses).should(exist).click();
    }

    public void subscribes() {
        System.out.println("Переходим на страницу подписок");
        $(subscribes).should(exist).click();
    }

    public void favoriteVacancies() {
        System.out.println("Переходим на страницу избранных вакансий");
        $(favoriteVacancies).should(exist).click();
    }

    public void blackList() {
        System.out.println("Переходим на страницу черного списка");
        $(blackList).should(exist).click();
    }

    public void promo() {
        System.out.println("Переходим на страницу промо");
        $(promo).should(exist).click();
    }

    public void profileSettings() {
        System.out.println("Переходим на страницу профиля");
        $(profileSettings).should(exist).click();
    }

    public void exit() {
        System.out.println("Выходим из профиля");
        $(exit).should(exist).click();
    }
}
