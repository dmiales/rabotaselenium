package utils;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;

import static com.codeborne.selenide.Configuration.browser;
import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;
import static cucumber.api.SnippetType.CAMELCASE;


@CucumberOptions(
        features = "src/test/java/features",
        glue = "components",
        tags = {"@FirstScenario,@SecondScenario"},
        plugin={"pretty", "html:target/cucumber-html-report",
                "json:target/cucumber-report.json"},
        strict = true)
public class RunnerTest extends AbstractTestNGCucumberTests {

    @Parameters({"browser", "resolution"})
    @BeforeClass
    public void startSession(@Optional("Chrome") String configBrowser, @Optional("1366x768") String configResolution) throws IOException {
        browser = configBrowser;
        String[] subStr = configResolution.split("x", 2);
        Preparation.X = Integer.valueOf(subStr[0]);
        Preparation.Y = Integer.valueOf(subStr[1]);
        Configuration.browser = configBrowser;
        //Configuration.browserSize = X + "x" + Y;
        Configuration.timeout = 30000;
        //browserStorage();
        clearReports();
    }

    @BeforeTest
    public void getMainPage() {
        open("https://europa.rabota.space");
    }

    @AfterSuite
    public void endSession() {
        LogEntries logEntries = WebDriverRunner.getWebDriver().manage().logs().get(LogType.BROWSER);
        for (LogEntry log : logEntries) {
            System.out.println(log.getTimestamp() + " " + log.getLevel() + " " + log.getMessage());
        }
        close();
    }

    @BeforeSuite
    public void clearReports() throws IOException {
        FileUtils.deleteDirectory(new File("build"));
        //FileUtils.deleteDirectory(new File("src//main//java//src//" + browser + "//res" + X + "x" + Y + "//Report"));
        //File folder = new File("src//main//java//src//" + browser + "//res" + X + "x" + Y + "//Report");
        //if (!folder.exists()) {
        //    folder.mkdirs();
        //}
    }
}