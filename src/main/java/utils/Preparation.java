package utils;

import org.openqa.selenium.JavascriptExecutor;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

import javax.imageio.ImageIO;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Configuration.browser;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class Preparation {

    public static Integer X;
    public static Integer Y;

    Map<String, String> myDynamicMap = new HashMap<>();

    public void setVar(String var, String value) {
        myDynamicMap.put(var, value);
    }

    public void getVar(String var) {
        System.out.println(myDynamicMap.get(var));
    }

    public void fixHead() {
        JavascriptExecutor js = ((JavascriptExecutor) getWebDriver());
        js.executeScript("document.getElementsByClassName('page-header_fixed')[0].style.position = \"absolute\";");
    }

    public void screenShot(Screenshot fpScreenshot, String page) throws Exception {
        File folderOriginal = new File("src/resources/" + browser + "//res" + X + "x" + Y + "//" + page + "//Original//");
        if (!folderOriginal.exists()) {
            folderOriginal.mkdirs();
        }
        File folderReport = new File("build//reports//" + browser + "//res" + X + "x" + Y + "//" + page + "//Scenario//");
        if (!folderReport.exists()) {
            folderReport.mkdirs();
        }

        File file = new File("src/resources/" + browser + "//res" + X + "x" + Y + "//" + page + "//Original//" + page + ".png");
        System.out.println("src/resources/" + browser + "//res" + X + "x" + Y + "//" + page + "//Original//" + page + ".png");
        System.out.println(file.exists());
        if (file.exists()) {
            ImageIO.write(fpScreenshot.getImage(), "PNG", new File("build//reports//" + browser + "//res" + X + "x" + Y + "//" + page + "//Scenario//" + page + ".png"));
            Screenshot expectedImage = new Screenshot(ImageIO.read(file));
            expectedImage.setIgnoredAreas(fpScreenshot.getIgnoredAreas());
            ImageDiff diff = new ImageDiffer().makeDiff(expectedImage, fpScreenshot);
            System.out.println(diff.hasDiff());
            if (diff.hasDiff()) {
                diff.getDiffSize();
                File diffFile = new File("build//reports//" + browser + "//res" + X + "x" + Y + "//" + page + ".png");
                ImageIO.write(diff.getMarkedImage(), "png", diffFile);
                System.out.println("Найдены отличия см." + page);
            }
        } else {
            File folderBuildOriginal = new File("src/resources/" + browser + "//res" + X + "x" + Y + "//" + page + "//Original//" + page + ".png");
            if (!folderBuildOriginal.exists()) {
                folderBuildOriginal.mkdirs();
            }
            ImageIO.write(fpScreenshot.getImage(), "PNG", folderBuildOriginal);
        }
    }
}
