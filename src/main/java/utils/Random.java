package utils;

public class Random {
    public static String getRandomString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        java.util.Random rnd = new java.util.Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String randomString = salt.toString();
        return randomString;
    }

    public static String getRandomSalary() {
        String salary = "" + getRandomNumberInRange(1000, 1000000);
        return salary;
    }

    private static int getRandomNumberInRange(int min, int max) {
        java.util.Random r = new java.util.Random();
        return r.ints(min, (max + 1)).findFirst().getAsInt();
    }

    public static String getRandomName(){
        String[] myString = new String[]{"Абрам", "Август", "Адам", "Адриан", "Аким", "Александр", "Алексей", "Альберт", "Ананий",
                "Анатолий", "Андрей", "Антон", "Антонин", "Аполлон", "Аркадий", "Арсений", "Артемий", "Артур",
                "Артём", "Афанасий", "Богдан", "Болеслав", "Борис", "Бронислав", "Вадим", "Валентин",
                "Валериан", "Валерий", "Василий", "Вениамин"};
        int n = (int)Math.floor(Math.random() * myString.length);
        return myString[n];
    }
    public static String getRandomFamily(){
        String[] myString = new String[]{"Смирнов", "Иванов", "Кузнецов", "Соколов", "Попов", "Лебедев", "Козлов", "Новиков", "Морозов",
                "Волков", "Соловьёв", "Васильев", "Зайцев", "Павлов", "Семёнов", "Голубев", "Виноградов", "Богданов",
                "Фёдоров", "Михайлов", "Беляев", "Тарасов", "Белов", "Комаров", "Орлов", "Киселёв", "Макаров",
                "Ковалёв", "Ильин", "Гусев", "Титов", "Кузьмин", "Кудрявцев", "Баранов", "Куликов", "Алексеев",
                "Яковлев", "Сорокин", "Сергеев", "Романов", "Захаров", "Борисов", "Королёв", "Герасимов", "Пономарёв",
                "Григорьев", "Лазарев", "Медведев", "Ершов", "Никитин", "Соболев", "Рябов", "Поляков", "Цветков",
                "Жуков", "Фролов", "Журавлёв", "Николаев", "Крылов", "Максимов", "Сидоров", "Осипов", "Белоусов",
                "Дорофеев", "Егоров", "Матвеев", "Бобров", "Дмитриев", "Калинин", "Анисимов", "Петухов", "Антонов",
                "Никифоров", "Веселов", "Филиппов", "Марков", "Большаков", "Суханов", "Миронов", "Ширяев",
                "Коновалов", "Шестаков", "Казаков", "Ефимов", "Денисов", "Громов", "Фомин", "Давыдов", "Мельников",
                "Блинов", "Колесников", "Карпов", "Афанасьев", "Власов", "Маслов", "Исаков"};
        int n = (int)Math.floor(Math.random() * myString.length);
        return myString[n];
    }
    public static String getRandomNameCompany(){
        String[] myString = new String[]{"Компания1", "Компания2", "Компания3", "Компания4"};
        int n = (int)Math.floor(Math.random() * myString.length);
        return myString[n];
    }

    public static String getRandomMobilePhoneHR()
    {
        int num1, num2, num3; //3 numbers in area code
        int set2, set3; //sequence 2 and 3 of the phone number

        java.util.Random generator = new java.util.Random();
        num1 = 9; //add 1 so there is no 0 to begin
        num2 = generator.nextInt(8); //randomize to 8 becuase 0 counts as a number in the generator
        num3 = generator.nextInt(8);

        set2 = generator.nextInt(643) + 100;

        //Sequence 3 of numebr
        // add 1000 so there will always be 4 numbers
        //8999 so it wont succed 9999 when the 1000 is added
        set3 = generator.nextInt(8999) + 1000;

        return "" + num1 + num2 + num3 + set2 + set3;
    }

    // public void gerRandomPassword() {
    //     Random r = new Random();
    //     String rus = "абвгдежзиклмн";
    //     String eng = "abcdefghijklmnopqrstuvwxyz";
    //     String dig = "0123456789";
    //     String smb = "!@#$%^&*()";

    //     String sum = eng + eng.toUpperCase() +rus+rus.toUpperCase()+
    //             dig+smb;
    //     char c = sum.charAt(r.nextInt(sum.length()));
    // return c;
    // }

    public static String getRandomPas() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefjdqe!@#$%^&*()";
        StringBuilder salt = new StringBuilder();
        java.util.Random rnd = new java.util.Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String randomString = salt.toString();
        return randomString;
    }


}