package components.employer;

import org.testng.annotations.Test;
import pages.employer.CrmLogin;

public class CrmLoginPage {
    CrmLogin crmLogin = new CrmLogin();

    @Test
    public void succesLoginCRM(String login,
                         String password){
       crmLogin.setLoginCRM(login);
       crmLogin.setPasswordCRM(password);
       crmLogin.clickSignInButton();
    }

}

