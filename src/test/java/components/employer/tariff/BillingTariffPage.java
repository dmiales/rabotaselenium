package components.employer.tariff;

import cucumber.api.java.en.Then;
import org.testng.annotations.Test;
import pages.employer.tariff.BillingTariff;

import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;


public class BillingTariffPage {

    @Test
    @Then("^Оформим заказ на покупку тарифа$")
    public void standartBilling(){
        BillingTariff billingTariff = new BillingTariff();
        billingTariff.paymentMethodCard();
        billingTariff.documentsJurist();
        billingTariff.clickOnButtonConfirm();
        sleep(3000);
        String pageUrl = url();
        assertThat(pageUrl, anyOf(containsString("https://demomoney.yandex.ru/payments/")));
    }
}
