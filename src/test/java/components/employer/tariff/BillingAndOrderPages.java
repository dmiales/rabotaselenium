package components.employer.tariff;

import components.employer.login.SuccessLoginHR;
import org.testng.annotations.Test;
import pages.employer.HrPage;
import pages.employer.billingAndOrder.BillingAndOrder;

public class BillingAndOrderPages {

    BillingAndOrder billingAndOrder = new BillingAndOrder();
    HrPage hrPage = new HrPage();
    SuccessLoginHR successLoginHR = new SuccessLoginHR();

    @Test
    public void getNumberOrder(){
        successLoginHR.oldAutorizationAnyData("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnOrderBill();
        billingAndOrder.getNumberOrder();

    }
}
