package components.employer.tariff;

import components.employer.HrPageClick;
import components.employer.login.SuccessLoginHR;
import cucumber.api.java.en.Then;
import org.testng.annotations.Test;
import pages.employer.tariff.TariffHrOrder;

import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class BuyTarrifLkPage {

    //добавление товара в корзину
    @Test
    @Then("^Добавим тариф в корзину$")
    public void addStandartVanacyToCart(){
        SuccessLoginHR successLoginHR = new SuccessLoginHR();
        TariffHrOrder tariffHrOrder = new TariffHrOrder();
        HrPageClick hrPageClick = new HrPageClick();
        successLoginHR.oldAutorizationAnyData("esb@rabota.ru", "MgX894Ex");
        hrPageClick.clickOnShop();
        tariffHrOrder.clickOnStandartVacancyIncrement();
        tariffHrOrder.buttonBuy();
        sleep(3000);
        String pageUrl = url();
        assertThat(pageUrl, equalTo("https://www.dione.rabota.space/v3_myPaidServiceEdit.html?action=essential"));
    }
}
