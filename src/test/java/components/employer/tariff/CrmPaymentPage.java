package components.employer.tariff;

import components.employer.CrmLoginPage;
import components.employer.login.SuccessLoginHR;
import cucumber.api.java.en.Then;
import org.testng.annotations.Test;
import pages.employer.HrPage;
import pages.employer.billingAndOrder.BillingAndOrder;
import pages.employer.tariff.CrmPayment;

import static com.codeborne.selenide.Selenide.*;

public class CrmPaymentPage {
    CrmPayment crmPayment = new CrmPayment();
    CrmLoginPage crmLoginPage = new CrmLoginPage();
    BillingAndOrderPages billingAndOrderPages = new BillingAndOrderPages();

    @Test
    @Then("^Оплатим тариф в CRM$")
    public void payment2(){
        String numberOrder, number;
        SuccessLoginHR successLoginHR = new SuccessLoginHR();
        HrPage hrPage = new HrPage();
        BillingAndOrder billingAndOrder = new BillingAndOrder();
       // successLoginHR.oldAutorizationAnyData("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnOrderBill();
        numberOrder = $x("(//div[@class='lblock_info_block_title fleft']/a[@class='title_link large'])[1]").getText();
        String[] parts = numberOrder.split("-");
        number = parts[parts .length-1];
        open("https://crm.dione.rabota.space");
        crmLoginPage.succesLoginCRM("esb@rabota.ru", "esb@rabota.ru");
        open("https://crm.dione.rabota.space/employer/company/full-view/1769569");
        sleep(3000);
        crmPayment.scroll();
        crmPayment.clickOnHistoryOrders();
       $x("//a[contains(text(), "+ number +" )]").click();
       switchTo().window(1);
        crmPayment.clickOrderStatus();
        crmPayment.clickReadyRun();
        crmPayment.clickOnDatePayment();
        crmPayment.clickSaveButton();
    }


    @Test
    public void payment(){

        billingAndOrderPages.getNumberOrder();
        open("https://crm.dione.rabota.space");
        crmLoginPage.succesLoginCRM("esb@rabota.ru", "esb@rabota.ru");
        open("https://crm.dione.rabota.space/employer/company/full-view/1769569");
        sleep(3000);
        crmPayment.clickOnHistoryOrders();
     //  System.out.println(number);
     //  $x("//a[contains(text(), ' " + number + "' )]").click();
        crmPayment.clickOnLastOrder();
        sleep(2000);
        crmPayment.clickOrderStatus();
        crmPayment.clickReadyRun();
        crmPayment.clickOnDatePayment();
        crmPayment.clickSaveButton();
    }
}
