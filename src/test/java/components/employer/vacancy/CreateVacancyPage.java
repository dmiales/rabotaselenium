package components.employer.vacancy;

import cucumber.api.java.en.Then;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CreateVacancyPage {
    pages.employer.vacancy.CreateVacancy createVacancy;
    pages.employer.login.SignInHr signInHr;
    pages.employer.HrPage hrPage;
    components.employer.login.SuccessLoginHR successLoginHR;

    //создание вакансии SPA
    @Test
    @Then("^Создадим вакансию$")
    public void CreateVacancySuccesSave(){
        createVacancy = new pages.employer.vacancy.CreateVacancy();
        signInHr = new pages.employer.login.SignInHr();
        hrPage = new pages.employer.HrPage();
        successLoginHR = new components.employer.login.SuccessLoginHR();
       // successLoginHR.oldAutorizationAnyData("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnCreateVacancy();
        createVacancy.titleVacancy("Кассир");
        createVacancy.salary("1000", "10000");
        createVacancy.specialization();
        createVacancy.experience();
        createVacancy.shedule();
        createVacancy.description();
        createVacancy.saveVacancy();
        sleep (3000);
        String pageUrl=url();
        assertThat(pageUrl, equalTo("https://www.dione.rabota.space/v3_vacancyList.html?action=drop"));
    }


    //создание вакансии Retro
    @Test
    public void CreateVacancySuccesSaveRetro(){
        createVacancy = new pages.employer.vacancy.CreateVacancy();
        signInHr = new pages.employer.login.SignInHr();
        hrPage = new pages.employer.HrPage();
        successLoginHR = new components.employer.login.SuccessLoginHR();
        // successLoginHR.oldAutorizationAnyData("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnCreateVacancyRetro();
        createVacancy.titleVacancy("Кассир");
        createVacancy.salary("1000", "10000");
        createVacancy.specialization();
        createVacancy.experience();
        createVacancy.shedule();
        createVacancy.workPlace("Москва");
        createVacancy.description();
        createVacancy.saveVacancy();
        sleep (3000);
        String pageUrl=url();
        assertThat(pageUrl, equalTo("https://www.dione.rabota.space/v3_vacancyList.html")); //старый ЛК
    }

    //пустые поля
    @Test
    public void CreateVacancyInvalid(){
        createVacancy = new pages.employer.vacancy.CreateVacancy();
        signInHr = new pages.employer.login.SignInHr();
        hrPage = new pages.employer.HrPage();
        successLoginHR = new components.employer.login.SuccessLoginHR();
        successLoginHR.oldAutorizationAnyData("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnCreateVacancy();
        createVacancy.saveVacancy();
        String titleError = createVacancy.titleError();
        String experienceError = createVacancy.experienceError();
        String descriptionError = createVacancy.descriptionError();
        String sheduleError = createVacancy.sheduleError();
        String rubrikError = createVacancy.rubrikError();
        assertThat("Укажите, пожалуйста, название вакансии.", equalTo(titleError));
        assertThat("Выберите желаемый опыт работы кандидатов.", equalTo(experienceError));
        assertThat("Укажите, пожалуйста, описание вакансии.", equalTo(descriptionError));
        assertThat("Выберите, пожалуйста, график.", equalTo(sheduleError));
        assertThat("Укажите, пожалуйста, профессиональную сферу", equalTo(rubrikError));
    }

    @Test
    public void SalaryFromInvalid(){
        createVacancy = new pages.employer.vacancy.CreateVacancy();
        signInHr = new pages.employer.login.SignInHr();
        hrPage = new pages.employer.HrPage();
        successLoginHR = new components.employer.login.SuccessLoginHR();
        successLoginHR.oldAutorizationAnyData("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnCreateVacancy();
        createVacancy.salary("10001", "1000");
        String salaryFrom = createVacancy.salaryFromError();
        assertThat("Нижний уровень зарплаты не может быть больше верхнего", equalTo(salaryFrom));
    }

    //TODO не понятно  поченму ошибка
        @Test
     public void SalaryFromInvalid2(){
        createVacancy = new pages.employer.vacancy.CreateVacancy();
        signInHr = new pages.employer.login.SignInHr();
        hrPage = new pages.employer.HrPage();
        successLoginHR = new components.employer.login.SuccessLoginHR();
        successLoginHR.oldAutorizationAnyData("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnCreateVacancy();
        createVacancy.titleVacancy("Кассир");
        createVacancy.salary("800", "10000");
        createVacancy.specialization();
        createVacancy.experience();
        createVacancy.shedule();
        createVacancy.description();
        createVacancy.saveVacancy();
        sleep(3000);
        String salaryFromError=createVacancy.salaryFromError2();
        assertThat("Попробуйте увеличить сумму", equalTo(salaryFromError));

        }
}
