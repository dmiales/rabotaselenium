package components.employer.vacancy;

import org.testng.annotations.Test;
import pages.employer.vacancy.VacanciesPagesFilter;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class VacanciesFilterClick {
    VacanciesPagesFilter vacanciesPages;
    components.employer.login.SuccessLoginHR successLoginHR;
    pages.employer.HrPage hrPage;

    //вкладка неопубликованные - фильтры показать
    @Test
    public void clickFiltersShow(){
        vacanciesPages = new VacanciesPagesFilter();
        successLoginHR = new components.employer.login.SuccessLoginHR();
        hrPage = new pages.employer.HrPage();
      //  successLoginHR.oldAutorizationAnyData("esb@rabota.ru", "MgX894Ex");
      //  sleep(3000);
        vacanciesPages.clickOnUnpublish();
        vacanciesPages.clickOnSearchCity();
        vacanciesPages.clickOnSearchRecruiter();
        vacanciesPages.clickOnSearchContactPerson();
        vacanciesPages.showButtonFilter();
        sleep(3000);
    }

    //неопубликовааные - пустые фильтры
     @Test
   public void clickFilterClear(){
       clickFiltersShow();
       String text;
       vacanciesPages.clearButtonFilter();
       sleep(2000);
     //  String textCleanCity = $x("//div[@class='card__text vacancies-filter__fields']/div[4]").getValue();
     //  System.out.println("a+"+textCleanCity+"+12");
       if(($x("//div[@class='card__text vacancies-filter__fields']/div[4]").getValue() == null)
       && ($x("//div[@class='card__text vacancies-filter__fields']/div[3]").getValue()==null)
       && ($x("//div[@class='card__text vacancies-filter__fields']/div[2]").getValue()==null)
       && ($x("//div[@class='card__text vacancies-filter__fields']/div[1]").getValue()==null)){
           text = "Поле пустое";
       }
       else text = "Фильтр не сброшен";
       assertThat("Поле пустое", equalTo(text));
   }
}
