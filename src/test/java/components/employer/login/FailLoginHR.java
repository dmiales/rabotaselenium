package components.employer.login;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.Test;
import utils.Random;

import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class FailLoginHR {
    pages.employer.HrPage hrPage;
    pages.employer.login.SignInHr signInHr;

    //неверный пароль
    @Test
    public void passwordWithIncorrectCredentials() {
        signInHr = new pages.employer.login.SignInHr();
        Selenide.open("https://hr.dione.rabota.space");
        signInHr.clickOnbuttonSignIn();
        String randomString = Random.getRandomString();
        signInHr.logIn("esb@rabota.ru", "MgX894Ex123");
        sleep(2000);
        String incorrectCredentials = signInHr.getErrorIncorrectCredentials();
        assertThat("Неверный логин или пароль", equalTo(incorrectCredentials) );
    }

    //неверный логин
    @Test
    public void logInWithIncorrectCredentials() {
        signInHr = new pages.employer.login.SignInHr();
        Selenide.open("https://hr.dione.rabota.space");
        signInHr.clickOnbuttonSignIn();
        String randomString = Random.getRandomString();
        signInHr.logIn("esb12@rabota.ru", "MgX894Ex");
        sleep(2000);
        String incorrectCredentials = signInHr.getErrorIncorrectCredentials();
        assertThat("Неверный логин или пароль", equalTo(incorrectCredentials) );
    }

    //нет аккаунта
    @Test
    public void noAccount() throws InterruptedException {
        signInHr = new pages.employer.login.SignInHr();
        Selenide.open("https://hr.dione.rabota.space");
        signInHr.clickOnbuttonSignIn();
        signInHr.clickOnNoAccount();
        sleep(2000);
        String pageURL = url();
        assertThat(pageURL, equalTo("https://hr.dione.rabota.space/sign-up?redirectTo=%2Fsign-in"));
    }

}
