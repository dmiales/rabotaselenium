package components.employer.login;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class SuccessLoginHR {
    pages.employer.HrPage hrPage;
    pages.employer.login.SignInHr signInHr;

    @Test
    public void signHr() {
        signInHr = new pages.employer.login.SignInHr();
        Selenide.open("https://hr.dione.rabota.space");
        signInHr.clickOnbuttonSignIn();
        signInHr.logIn("esb@rabota.ru", "MgX894Ex");
        sleep(5000);
        String userHRName = hrPage.getHRName();
        assertThat("Елизавета Белых", equalTo(userHRName));
    }

    @Test
    public void oldAutorizhation (){
        hrPage = new pages.employer.HrPage();
        signInHr = new pages.employer.login.SignInHr();
        Selenide.open("https://hr.dione.rabota.space");
        hrPage.clickOnButtonCooke();
        sleep(10000);
        signInHr.oldEmail("esb@rabota.ru");
        signInHr.oldPassword("MgX894Ex");
        signInHr.oldButton();
    }

    //можно ли так? необходим для быстрой авторизации в смоке
    @Test
    public void oldAutorizationAnyData(String email,
                                       String password){
        hrPage = new pages.employer.HrPage();
        signInHr = new pages.employer.login.SignInHr();
        Selenide.open("https://hr.dione.rabota.space");
        hrPage.clickOnButtonCooke();
        sleep(7000);
        signInHr.oldEmail(email);
        signInHr.oldPassword(password);
        signInHr.oldButton();

    }
}