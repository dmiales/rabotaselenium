package components.employer.register;

import com.codeborne.selenide.Selenide;
import cucumber.api.java.en.Then;
import org.testng.annotations.Test;
import pages.employer.HrPage2016;
import pages.employer.login.Registration;
import pages.employer.login.SignInHr;
import utils.Random;

import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class SuccesRegisterHR {

    @Test
    @Then("^Зарегистрируемся как работодатель$")
    public void validRegistration() {
        Registration registration = new Registration();
        HrPage2016 hrPage2016 = new HrPage2016();
        SignInHr signInHr = new SignInHr();
        Selenide.open("http://dione.rabota.space/hr");
        String nameHR = Random.getRandomName();
        String familyHR = Random.getRandomFamily();
        String randomString = Random.getRandomString();
        String emailHR = randomString + "@ya.ru";
        String telephoneHr = Random.getRandomMobilePhoneHR();
        String passwordHR = Random.getRandomPas(); //нужно переделать генерацию пароля
        hrPage2016.clickOnSign();
        signInHr.oldRegistration();
        registration.innCompanyHR("7707083893");
        registration.validRegistration(nameHR, familyHR, emailHR, "123QWEqwe!", telephoneHr);
        registration.nextOnClick();
        sleep(5000);
        String pageURL = url();
        String assertion = "https://dione.rabota.space/v3_joinEmployer.html?action=filter&inn=7707083893&region_id=3&name=%D0%A1%D0%B1%D0%B5%D1%80%D0%B1%D0%B0%D0%BD%D0%BA%20%D1%80%D0%BE%D1%81%D1%81%D0%B8%D0%B8&type=%D0%9F%D0%90%D0%9E";
        assertThat(pageURL, equalTo(assertion));
    }

    //регистрация с уже подтвержденным ИНН - кадровое агентство
    @Test
    public void validRegistrationAgency() {
        Registration registration = new Registration();
        Selenide.open("http://hr.dione.rabota.space/v3_registerEmployer.html");
        String nameHR = Random.getRandomName();
        String familyHR = Random.getRandomFamily();
        String randomString = Random.getRandomString();
        String emailHR = randomString + "@ya.ru";
        String telephoneHr = Random.getRandomMobilePhoneHR();
        String passwordHR = Random.getRandomString(); //нужно переделать генерацию пароля
        registration.agencyOnCLick();
        registration.innCompanyHR("7707083893");
        registration.validRegistration(nameHR, familyHR, emailHR, "123QWEqwe!", telephoneHr);
        registration.nextOnClick();
        sleep(5000);
        String pageURL = url();
        assertThat(pageURL, equalTo("https://dione.rabota.space/v3_joinEmployer.html?action=filter&inn=7707083893&region_id=3&name=%D0%A1%D0%B1%D0%B5%D1%80%D0%B1%D0%B0%D0%BD%D0%BA%20%D1%80%D0%BE%D1%81%D1%81%D0%B8%D0%B8&type=%D0%9F%D0%90%D0%9E"));
    }

    //  //регистрация с новым ИНН - сделать генерацию ИНН

    //кнопка войти с регистрации
    @Test
    public void sign() {
        Registration registration = new Registration();
        Selenide.open("http://hr.dione.rabota.space/v3_registerEmployer.html");
        registration.signOnClick();
        sleep(4000);
        String pageURL = url();
        assertThat(pageURL, equalTo("https://www.dione.rabota.space/v3_login.html"));
    }

   @Test
   public void signSber(){
       Registration registration = new Registration();
       Selenide.open("http://hr.dione.rabota.space/v3_registerEmployer.html");
       registration.sberButtonClick();
       sleep(5000);
       String pageURL = url();
      assertThat(pageURL, anyOf(containsString("https://sbi.sberbank.ru:9443/ic/sso/#/login?redirect_uri=https%3A%2F%2Fsocial.dione.rabota.space%2Fend%")));
 }

}