package components.employer.register;
import com.codeborne.selenide.Selenide;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ErrorRegisterHR {
    pages.employer.login.Registration registration;

    //все поля не валидны
    @Test
    public void errors(){
        registration = new pages.employer.login.Registration();
        Selenide.open("http://hr.dione.rabota.space/v3_registerEmployer.html");
        registration.errors("1111", "qwer", "123QWEq","909366670");
        registration.nextOnClick();
        String errorsInn= registration.getErrorINN();
        String errorsEmail = registration.getEmail();
        String errorsPassword = registration.getPasswordMin();
        assertThat("ИНН может состоять только из 10 или 12 цифр", equalTo(errorsInn));
        assertThat("Введен некорректный почтовый адрес", equalTo(errorsEmail));
        assertThat("Пароль должен содержать 8 знаков и более", equalTo(errorsPassword));
    }

    //невалидный инн, email с двумя @, пароль из восьми цифр
    @Test
    public void errors2(){
        registration = new pages.employer.login.Registration();
        Selenide.open("http://hr.dione.rabota.space/v3_registerEmployer.html");
        registration.errors("111111111111", "rabota@@mail.ru", "11111111","909366670");
        registration.nextOnClick();
        String errorsInn= registration.getIncorrectInn();
        String errorsEmail = registration.getEmail();
        String errorsPassword = registration.getPasswordNum();
        assertThat("Неправильный ИНН", equalTo(errorsInn));
        assertThat("Введен некорректный почтовый адрес", equalTo(errorsEmail));
        assertThat("Пароль должен содержать хотя бы одну строчную букву (a-z/а-я)", equalTo(errorsPassword));
    }

    //инн из символов, email без точки, пароль из цифры и прописной буквы
    @Test
    public void errors3(){
        registration = new pages.employer.login.Registration();
        Selenide.open("http://hr.dione.rabota.space/v3_registerEmployer.html");
        registration.errors("test", "test@mailru","1123test", "909366670");
        registration.nextOnClick();
        String errorsInn = registration.getIncorrectInnSym();
        String errorsEmail = registration.getEmail();
        String errorsPassword =registration.getPasswordNum2();
        assertThat("ИНН может состоять только из цифр", equalTo(errorsInn));
        assertThat("Введен некорректный почтовый адрес", equalTo(errorsEmail));
        assertThat("Пароль должен содержать хотя бы одну прописную букву (A-Z/А-Я)", equalTo(errorsPassword));
    }

}

