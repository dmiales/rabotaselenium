package components.employer;
import com.codeborne.selenide.Selenide;
import cucumber.api.java.en.Then;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class HrPageClick {
    pages.employer.HrPage hrPage;
    pages.employer.login.SignInHr signInHr;

    @Test
    @Then("^Перейдем в личный кабинет работодателя$")
    public void openHR (){
        Selenide.open("https://hr.dione.rabota.space");
    }

    //подправить. все разом не запустятся
    @Test
    public void clickVacancy() {
        hrPage = new pages.employer.HrPage();
        signInHr = new pages.employer.login.SignInHr();
      //  signInHr.succesSign("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnMyVacancies();
        sleep(1000);
        String pageURL = url();
        assertThat(pageURL, equalTo("https://hr.dione.rabota.space/vacancies"));
    }
    @Test
    public void clickResponses(){
        hrPage = new pages.employer.HrPage();
        signInHr = new pages.employer.login.SignInHr();
      //  signInHr.succesSign("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnMyResponses();
        sleep(4000);
        String pageURL=url();
        assertThat(pageURL,equalTo("https://www.dione.rabota.space/responses"));
    }

    //TODO регулярки
    @Test
    public void clickOnFindResume(){
        hrPage = new pages.employer.HrPage();
        signInHr = new pages.employer.login.SignInHr();
    //    signInHr.succesSign("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnFindResume();
        sleep(3000);
        //  String pageURL = url();
        //  assertThat(pageURL, equalTo(Config.host_2 + "/v3_searchResumeByParamsResults.html?"));
    }

    @Test
    public void clickOnBaseResume(){
        hrPage = new pages.employer.HrPage();
        signInHr = new pages.employer.login.SignInHr();
   //     signInHr.succesSign("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnBaseResume();
        sleep(3000);
        String pageUrl = url();
        assertThat(pageUrl,equalTo("https://www.dione.rabota.space/v3_myResumeDatabase.html"));
    }
    @Test
    public void clickOnShop(){
        hrPage = new pages.employer.HrPage();
        signInHr = new pages.employer.login.SignInHr();
     //   signInHr.succesSign("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnShop();
        sleep(3000);
        String pageUrl=url();
        assertThat(pageUrl,equalTo("https://www.dione.rabota.space/v3_shop.html"));
    }
    @Test
    public void clickOnNyCompany(){
        hrPage = new pages.employer.HrPage();
        signInHr = new pages.employer.login.SignInHr();
    //    signInHr.succesSign("esb@rabota.ru", "MgX894Ex");
        hrPage.clickOnMyCompany();
        sleep(2000);
        String pageUrl=url();
        assertThat(pageUrl,equalTo("https://www.dione.rabota.space/v3_employerMain.html?action=edit"));
    }



}
