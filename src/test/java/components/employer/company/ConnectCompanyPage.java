package components.employer.company;

import cucumber.api.java.en.Then;
import org.testng.annotations.Test;
import pages.employer.company.ConnectCompany;

public class ConnectCompanyPage {
    ConnectCompany connectCompany;

    @Test
    @Then("^Присоединимся к компании$")
    public void connectCompany() {
        connectCompany = new ConnectCompany();
        connectCompany.connectCompany();
    }

}
