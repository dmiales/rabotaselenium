package components.employer.company;

import cucumber.api.java.en.Then;
import org.testng.annotations.Test;
import pages.employer.company.NewCompany;

public class NewCompanyPage {

    @Test
    @Then("^Создадим компанию$")
    public void newCompanyCreate(){
        NewCompany newCompany = new NewCompany();
        newCompany.clickOnTabNewCompany();
        newCompany.companyName("Та самая");
        newCompany.industry();
        newCompany.sendDocuments();
        newCompany.clickOnNextButton();
    }

}
