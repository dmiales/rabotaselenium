package components;

import cucumber.api.java.en.Then;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.Sandwich;
import pages.worker.login.SignIn;
import utils.Preparation;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class General {

    Preparation Preparation = new Preparation();


    @Test(description = "Подготовка для тестов, зависящих от страницы логина")
    public void GetLoginPage() {
        MainPage MainPage = new MainPage();

        MainPage.getLoginPage();
    }

    @Test(description = "Подготовка для тестов, зависящих от страницы регистрации")
    public void GetRegisterPage() {
        SignIn SignIn = new SignIn();

        GetLoginPage();
        SignIn.getRegisterPage();
    }

    @Test
    @Then("^запишем в переменную (.+) значение (.+)$")
    public void setVar(String var, String value) {
        Preparation.setVar(var, value);
    }

    @Test
    @Then("^выведем переменную (.+)$")
    public void getVar(String var) {
        Preparation.getVar(var);
    }

    @Test
    @Then("^разлогинимся$")
    public void Logout() {
        Sandwich Sandwich = new Sandwich();

        Sandwich.sandwichExpand();
        Sandwich.exit();
    }

    @Test(description = "эта штука уберет тулбар")
    public void browserStorage() {
        JavascriptExecutor js = ((JavascriptExecutor) getWebDriver());
        js.executeScript("window.localStorage.setItem('symfony/profiler/toolbar/displayState','none');");
        js.executeScript("window.localStorage.setItem('nps-anketolog','{\"expiredAtMs\":1576234873123,\"item\":{\"routeChanges\":5,\"seenBefore\":true},\"version\":null}');");
    }
}