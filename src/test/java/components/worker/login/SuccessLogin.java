package components.worker.login;

import cucumber.api.java.en.Then;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.worker.login.SignIn;

public class SuccessLogin {

    @Test
    @Then("^зайдем на главную и выполним авторизацию с корректным паролем$")
    public void FullSuccessLoginScenario() throws Exception {
        MainPage MainPage = new MainPage();

        MainPage.getLoginPage();
        ShortSuccessLoginScenario();
    }

    @Test
    @Then("^выполним авторизацию с корректным паролем$")
    public void ShortSuccessLoginScenario() throws Exception {
        SignIn SignIn = new SignIn();

        SignIn.screenShot();
        SignIn.signInSuccess();
    }
}