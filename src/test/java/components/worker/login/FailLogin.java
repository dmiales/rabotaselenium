package components.worker.login;

import cucumber.api.java.en.Then;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.worker.login.SignIn;

public class FailLogin {

    @Test
    @Then("^зайдем на главную и выполним авторизацию с некорректным паролем$")
    public void FullFailLoginScenario() {
        MainPage MainPage = new MainPage();

        MainPage.getLoginPage();
        ShortFailLoginScenario();
    }

    @Test
    @Then("^выполним авторизацию с некорректным паролем$")
    public void ShortFailLoginScenario() {
        SignIn SignIn = new SignIn();

        SignIn.signInFail();
    }
}