package components.worker.login;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.worker.login.SocialNetworks;

public class SuccessSocialLogin {

    @Test(groups = "social")
    public void vkAuthorization() {
        SocialNetworks SocialNetworks = new SocialNetworks();

        SocialNetworks.clickSocialButton(SocialNetworks.vkSocialButton);
        SocialNetworks.mockAuthorization(SocialNetworks.vkUserId);
        String userName = SocialNetworks.getUserName();
        Assert.assertEquals("Илья Орджалиев", userName);
    }

    @Test(groups = "social")
    public void fbAuthorization() {
        SocialNetworks SocialNetworks = new SocialNetworks();

        SocialNetworks.clickSocialButton(SocialNetworks.fbSocialButton);
        SocialNetworks.mockAuthorization(SocialNetworks.fbUserId);
        String userName = SocialNetworks.getUserName();
        Assert.assertEquals("Наташа Воробьева", userName);
    }

    @Test(groups = "social")
    public void okAuthorization() {
        SocialNetworks SocialNetworks = new SocialNetworks();

        SocialNetworks.clickSocialButton(SocialNetworks.okSocialButton);
        SocialNetworks.mockAuthorization(SocialNetworks.okUserId);
        String userName = SocialNetworks.getUserName();
        Assert.assertEquals("Алан Кодзаев", userName);
    }

    @Test(groups = "social")
    public void yandexAuthorization() {
        SocialNetworks SocialNetworks = new SocialNetworks();

        SocialNetworks.clickSocialButton(SocialNetworks.yandexSocialButton);
        SocialNetworks.mockAuthorization(SocialNetworks.yandexUserId);
        String userName = SocialNetworks.getUserName();
        Assert.assertEquals("Евгений Саламонов", userName);
    }

    @Test(groups = "social")
    public void mailAuthorization() {
        SocialNetworks SocialNetworks = new SocialNetworks();

        SocialNetworks.clickSocialButton(SocialNetworks.mailSocialButton);
        SocialNetworks.mockAuthorization(SocialNetworks.mailUserId);
        String userName = SocialNetworks.getUserName();
        Assert.assertEquals("Евгений Полинов", userName);
    }

    @Test(groups = "social")
    public void googleAuthorization() {
        SocialNetworks SocialNetworks = new SocialNetworks();

        SocialNetworks.clickSocialButton(SocialNetworks.googleSocialButton);
        SocialNetworks.mockAuthorization(SocialNetworks.googleUserId);
        String userName = SocialNetworks.getUserName();
        Assert.assertEquals("Вячеслав Смирницкий", userName);
    }
}