package components.worker.resume;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.worker.login.SignIn;
import pages.worker.resume.ResumePage;
import pages.worker.resume.Sections.*;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class SuccessResume {

    @Test
    public void FullResumeScenario() throws Exception {
        MainPage MainPage = new MainPage();
        SignIn SignIn = new SignIn();
        pages.worker.login.Register Register = new pages.worker.login.Register();

        MainPage.getLoginPage();
        SignIn.getRegisterPage();
        Register.register();
        ShortResumeScenario();
    }

    @Test
    public void ShortResumeScenario() throws Exception {
        ExtraSection ExtraSection = new ExtraSection();
        //CertificateSection CertificateSection = new CertificateSection(driver);
        DrivingExperienceSection DrivingExperienceSection = new DrivingExperienceSection();
        EducationSection EducationSection = new EducationSection();
        ExperienceSection ExperienceSection = new ExperienceSection();
        LanguageSection LanguageSection = new LanguageSection();
        MainSection MainSection = new MainSection();
        //PortfolioSection PortfolioSection = new PortfolioSection(driver);
        ProfessionalSkillsSection ProfessionalSkillsSection = new ProfessionalSkillsSection();
        WishesSection WishesSection = new WishesSection();
        ResumePage ResumePage = new ResumePage();

        JavascriptExecutor js = ((JavascriptExecutor) getWebDriver());

        WebDriverWait wait = new WebDriverWait(getWebDriver(), 10);
        wait.until(ExpectedConditions.urlContains("my-resume/add"));

        ResumePage.screenShot();
        MainSection.fillMainSection();
        js.executeScript("window.scrollTo(0,800)");
        WishesSection.fillWishesSection();
        js.executeScript("window.scrollTo(0,1200)");
        ExperienceSection.fillExperienceSection();
        js.executeScript("window.scrollTo(0,1600)");
        EducationSection.fillEducationSection();
        js.executeScript("window.scrollTo(0,2000)");
        ProfessionalSkillsSection.fillProfessionalSkillsSection();
        js.executeScript("window.scrollTo(0,2400)");
        LanguageSection.fillLanguageSection();
        js.executeScript("window.scrollTo(0,2800)");
        DrivingExperienceSection.fillDrivingExperienceSection();
        js.executeScript("window.scrollTo(0,3200)");
        ExtraSection.fillExtraSection();
        js.executeScript("window.scrollTo(0,5000)");
        //CertificateSection.fillCertificateSection();
        //PortfolioSection.fillPortfolioSection();

        ResumePage.screenShotFull();

        ResumePage.saveResume();
    }
}