package components.worker.vacancies;

import cucumber.api.java.en.Then;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.worker.SearchPage;
import pages.worker.VacancyPage;
import components.worker.resume.SuccessResume;
import utils.Preparation;

import static com.codeborne.selenide.Selenide.open;

public class Vacancy extends Preparation {

    @Test
    @Then("^нажать кнопку регистрации1$")
    public void FullVacancyScenario() throws Exception {
        SuccessResume SuccessResume = new SuccessResume();
        MainPage MainPage = new MainPage();
        SearchPage SearchPage = new SearchPage();

        SuccessResume.FullResumeScenario();
        MainPage.getVacanciesPage();
        SearchPage.getRandomVacancy();
        ShortVacancyScenario();
    }

    @Test
    public void ShortVacancyScenario() throws Exception {
        VacancyPage VacancyPage = new VacancyPage();

        VacancyPage.getApiVacancy();
        VacancyPage.checkAllFields();
        VacancyPage.checkSimilar();
        VacancyPage.checkAllFeatures();
    }

    @Test
    public void CheckTestVacancy() throws Exception {
        VacancyPage VacancyPage = new VacancyPage();

        //VacancyPage.getApiVacancy();
        //VacancyPage.checkAllFields();
        //VacancyPage.checkSimilar();
        open("https://titan.rabota.space/vacancy/43365860/");
        VacancyPage.checkAllFeatures();
    }

    @Test
    public void ShortFromMainVacancyScenario() throws Exception {
        SearchPage SearchPage = new SearchPage();

        SearchPage.getRandomVacancy();
        ShortVacancyScenario();
    }
}