package components.worker.register;

import org.testng.annotations.Test;
import pages.MainPage;
import pages.worker.login.SignIn;

public class SuccessRegister {

    @Test
    public void FullRegisterScenario() throws Exception {
        MainPage MainPage = new MainPage();
        SignIn SignIn = new SignIn();

        MainPage.getLoginPage();
        SignIn.getRegisterPage();
        ShortRegisterScenario();
    }

    @Test
    public void ShortRegisterScenario() throws Exception {
        pages.worker.login.Register Register = new pages.worker.login.Register();

        Register.screenShot();
        Register.register();
    }
}