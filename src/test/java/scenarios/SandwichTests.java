package scenarios;

import org.testng.annotations.Test;
import pages.MainPage;
import pages.Sandwich;
import pages.worker.blacklist.BlackListPage;
import pages.worker.favorites.FavoritesVacanciesPage;
import pages.worker.login.SignIn;
import pages.worker.profile.ProfilePage;
import pages.worker.promo.PromoPage;
import pages.worker.responses.ResponsesPage;
import pages.worker.resumes.MyResumesPage;
import pages.worker.subscriptions.SubscriptionsPage;
import utils.Preparation;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class SandwichTests extends Preparation {

    @Test
    public void FullSandwichTestsScenario() throws Exception {
        MainPage MainPage = new MainPage();
        SignIn SignIn = new SignIn();

        MainPage.getLoginPage();
        SignIn.signInSuccess();
        ShortSandwichTestsScenario();
    }

    @Test
    public void ShortSandwichTestsScenario() throws Exception {
        Sandwich Sandwich = new Sandwich();
        BlackListPage BlackListPage = new BlackListPage();
        FavoritesVacanciesPage FavoritesVacanciesPage = new FavoritesVacanciesPage();
        MyResumesPage MyResumesPage = new MyResumesPage();
        ProfilePage ProfilePage = new ProfilePage();
        PromoPage PromoPage = new PromoPage();
        ResponsesPage ResponsesPage = new ResponsesPage();
        SubscriptionsPage SubscriptionsPage = new SubscriptionsPage();

        //Sandwich.sandwichExpand();
        //getWebDriver().navigate().refresh(); //обновляем страницу, но это баг

        Sandwich.sandwichExpand();
        Sandwich.resumes();
        MyResumesPage.screenShot();
        getWebDriver().navigate().back();

        Sandwich.sandwichExpand();
        Sandwich.responses();
        ResponsesPage.screenShot();
        getWebDriver().navigate().back();

        Sandwich.sandwichExpand();
        Sandwich.subscribes();
        SubscriptionsPage.screenShot();
        getWebDriver().navigate().back();

        Sandwich.sandwichExpand();
        Sandwich.favoriteVacancies();
        FavoritesVacanciesPage.screenShot();
        getWebDriver().navigate().back();

        Sandwich.sandwichExpand();
        Sandwich.blackList();
        BlackListPage.screenShot();
        getWebDriver().navigate().back();

        Sandwich.sandwichExpand();
        Sandwich.promo();
        PromoPage.screenShot();
        getWebDriver().navigate().back();

        Sandwich.sandwichExpand();
        Sandwich.profileSettings();
        //getWebDriver().navigate().refresh();//нужно, чтобы убрать этот опрос
        //getWebDriver().navigate().refresh();//нужно, чтобы убрать этот опрос
        //getWebDriver().navigate().refresh();//нужно, чтобы убрать этот опрос
        ProfilePage.screenShot();
        getWebDriver().navigate().back();

        Sandwich.sandwichExpand();
        Sandwich.exit();
    }
}