package scenarios;

import components.employer.company.ConnectCompanyPage;
import components.employer.company.NewCompanyPage;
import components.employer.login.SuccessLoginHR;
import components.employer.register.SuccesRegisterHR;
import components.employer.tariff.BillingTariffPage;
import components.employer.tariff.BuyTarrifLkPage;
import components.employer.vacancy.CreateVacancyPage;
import components.employer.vacancy.VacanciesFilterClick;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;

public class SmokeTestHR {

    //регистрация с созданием компании, создание вакансии
    @Test
    public void smoke(){
        SuccesRegisterHR succesRegisterHR = new SuccesRegisterHR();
        CreateVacancyPage createVacancyPage = new CreateVacancyPage();
        ConnectCompanyPage connectCompanyPage = new ConnectCompanyPage();
        NewCompanyPage newCompanyPage = new NewCompanyPage();
        succesRegisterHR.validRegistration();
        //connectCompanyPage.connectCompany();
        newCompanyPage.newCompanyCreate();
        sleep (3000);
        createVacancyPage.CreateVacancySuccesSaveRetro();
    }

    //авторизация и создание вакансии
    @Test
    public void autorizVacancy(){
        SuccessLoginHR successLoginHR = new SuccessLoginHR();
        CreateVacancyPage createVacancyPage = new CreateVacancyPage();
        VacanciesFilterClick vacanciesFilterClick = new VacanciesFilterClick();
        BillingTariffPage billingTariffPage = new BillingTariffPage();
        BuyTarrifLkPage buyTarrifLkPage = new BuyTarrifLkPage();
        successLoginHR.oldAutorizhation();
        createVacancyPage.CreateVacancySuccesSave();
        vacanciesFilterClick.clickFiltersShow();
        vacanciesFilterClick.clickFilterClear();
        buyTarrifLkPage.addStandartVanacyToCart();
        billingTariffPage.standartBilling();
    }
}
