package scenarios;

import components.General;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import pages.MainPage;
import pages.worker.SearchPage;
import components.worker.login.FailLogin;
import components.worker.login.SuccessLogin;
import components.worker.login.SuccessSocialLogin;
import components.worker.register.SuccessRegister;
import components.worker.resume.SuccessResume;
import components.worker.vacancies.Vacancy;
import utils.Preparation;

import static com.codeborne.selenide.Selenide.*;

public class SmokeTests extends Preparation {

    @Test(description = "Проверка всего")
    public void AllSmokeTests() throws Exception {
        FailLogin FailLogin = new FailLogin();
        SuccessLogin SuccessLogin = new SuccessLogin();
        SuccessRegister SuccessRegister = new SuccessRegister();
        SuccessResume SuccessResume = new SuccessResume();
        Vacancy Vacancy = new Vacancy();
        General General = new General();

        FailLogin.FullFailLoginScenario(); //проверка ошибок
        SuccessLogin.ShortSuccessLoginScenario(); //успешный логин
        General.Logout();

        SuccessRegister.FullRegisterScenario();
        SuccessResume.ShortResumeScenario();

        SmokeMap();

        SmokeSearchFilters();
        //Preparation.getMainPage();

        Vacancy.ShortFromMainVacancyScenario();

        SmokeSandwich();

        AllSocialNetworksLogin();
    }

    @Test(description = "Отдельная проверка страниц 2016")
    public void SmokeSandwich() throws Exception {
        SandwichTests SandwichTests = new SandwichTests();

        SandwichTests.ShortSandwichTestsScenario();
    }

    @Test(description = "Отдельная проверка резюме")
    public void SmokeResume() throws Exception {
        SuccessRegister SuccessRegister = new SuccessRegister();
        SuccessResume SuccessResume = new SuccessResume();

        SuccessRegister.FullRegisterScenario();
        SuccessResume.ShortResumeScenario();
    }

    @Test(description = "Отдельная проверка смены региона")
    public void SmokeMap() throws InterruptedException {
        MainPage MainPage = new MainPage();

        MainPage.fillLocation();
    }

    @Test(description = "Отдельная проверка фильтров")
    public void SmokeSearchFilters() {
        MainPage MainPage = new MainPage();

        SearchPage SearchPage = new SearchPage();

        MainPage.getVacanciesPage();
        SearchPage.fillSearchFilters();
    }

    @Test(description = "Отдельная проверка сортировок")
    public void SmokeSearchSorts() {
        MainPage MainPage = new MainPage();
        SearchPage SearchPage = new SearchPage();

        MainPage.getVacanciesPage();
        SearchPage.checkAllSorts();
    }

    @Test(description = "Отдельная проверка вакансии")
    public void SmokeRandomVacancy() throws Exception {
        Vacancy Vacancy = new Vacancy();

        Vacancy.FullVacancyScenario();
    }

    @Test(description = "Отдельная проверка определенной вакансии")
    public void SmokeTestVacancy() throws Exception {
        Vacancy Vacancy = new Vacancy();

        Vacancy.CheckTestVacancy();
    }

    @Test(description = "Проверка всех соц. сетей")
    public void AllSocialNetworksLogin() {
        SuccessSocialLogin SL = new SuccessSocialLogin();
        Preparation P = new Preparation();
        General General = new General();

        //P.GetLoginPage();
        //SL.vkAuthorization();
        //P.Logout();

        //P.GetLoginPage();
        //SL.fbAuthorization();
        //P.Logout();
//
        //P.GetLoginPage();
        //SL.okAuthorization();
        //P.Logout();
//
        //P.GetLoginPage();
        //SL.yandexAuthorization();
        //P.Logout();
//
        //P.GetLoginPage();
        //SL.mailAuthorization();
        //P.Logout();

        General.GetLoginPage();
        SL.googleAuthorization();
        General.Logout();
    }

    @Test(description = "Эксперимент с фреймом для Лизы")
    public void SmokeM1ap() throws Exception {

        open("http://titan.rabota.space/v3_myVacancy.html?action=create");
        By login = By.xpath("(//form/div[contains(@class, 'logpass_box')]/input)[1]");
        By pass = By.xpath("(//form/div[contains(@class, 'logpass_box')]/input)[2]");
        By btn = By.xpath("//input[contains(@class, 'login_btn')]");
        $(login).sendKeys("esb@rabota.ru");
        $(pass).sendKeys("MgX894Ex");
        $(btn).click();


        By frame = By.xpath("//tr/td[../td/text()='Описание вакансии']//iframe");
        $(frame).scrollTo();
        System.out.println($(frame));
        switchTo().frame($(frame));
        By inFrame = By.xpath("//body");
        System.out.println($(inFrame).innerHtml());

        $(inFrame).sendKeys("aaaaaaaaaaa");
        sleep(10000);

        switchTo().defaultContent();
    }

}